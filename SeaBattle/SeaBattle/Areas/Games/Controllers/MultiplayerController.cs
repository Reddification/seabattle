﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SeaBattle.Areas.Games.Controllers
{
    public class MultiplayerController : Controller
    {
        // GET: Games/Multiplayer
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Reconnect(Guid token)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Connect()
        {
            return View();
        }
    }
}
namespace SeaBattle.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attacks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        xCoord = c.Int(nullable: false),
                        yCoord = c.Int(nullable: false),
                        Missed = c.Boolean(nullable: false),
                        PlayerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.PlayerId, cascadeDelete: false)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "dbo.Players",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nickname = c.String(nullable: false, maxLength: 10),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Nickname, unique: true);
            
            CreateTable(
                "dbo.Battles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Player1Id = c.Int(nullable: false),
                        Player2Id = c.Int(nullable: false),
                        Player1Token = c.Guid(nullable: false),
                        Player2Token = c.Guid(nullable: false),
                        BattleResult = c.Int(nullable: false),
                        Started = c.DateTime(nullable: false),
                        Finished = c.DateTime(nullable: false),
                        Player_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.Player1Id, cascadeDelete: false)
                .ForeignKey("dbo.Players", t => t.Player2Id, cascadeDelete: false)
                .ForeignKey("dbo.Players", t => t.Player_Id)
                .Index(t => t.Player1Id)
                .Index(t => t.Player2Id)
                .Index(t => t.Player1Token, unique: true)
                .Index(t => t.Player2Token, unique: true)
                .Index(t => t.Player_Id);
            
            CreateTable(
                "dbo.Ships",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PlayerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.PlayerId, cascadeDelete: false)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "dbo.ShipUnits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ShipId = c.Int(nullable: false),
                        xCoord = c.Byte(nullable: false),
                        yCoord = c.Byte(nullable: false),
                        Destroyed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ships", t => t.ShipId, cascadeDelete: false)
                .Index(t => t.ShipId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ShipUnits", "ShipId", "dbo.Ships");
            DropForeignKey("dbo.Ships", "PlayerId", "dbo.Players");
            DropForeignKey("dbo.Battles", "Player_Id", "dbo.Players");
            DropForeignKey("dbo.Battles", "Player2Id", "dbo.Players");
            DropForeignKey("dbo.Battles", "Player1Id", "dbo.Players");
            DropForeignKey("dbo.Attacks", "PlayerId", "dbo.Players");
            DropIndex("dbo.ShipUnits", new[] { "ShipId" });
            DropIndex("dbo.Ships", new[] { "PlayerId" });
            DropIndex("dbo.Battles", new[] { "Player_Id" });
            DropIndex("dbo.Battles", new[] { "Player2Token" });
            DropIndex("dbo.Battles", new[] { "Player1Token" });
            DropIndex("dbo.Battles", new[] { "Player2Id" });
            DropIndex("dbo.Battles", new[] { "Player1Id" });
            DropIndex("dbo.Players", new[] { "Nickname" });
            DropIndex("dbo.Attacks", new[] { "PlayerId" });
            DropTable("dbo.ShipUnits");
            DropTable("dbo.Ships");
            DropTable("dbo.Battles");
            DropTable("dbo.Players");
            DropTable("dbo.Attacks");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SeaBattle.Models
{
    public enum Outcome { Player1Won=1, Player2Won=2, Player1Disconnected=3, Player2Disconnected=4}

    public class Battle
    {
        [Key]
        public int Id { get; set; }

        public Player Player1 { get; set; }
        public Player Player2 { get; set; }
        public int Player1Id { get; set; }
        public int Player2Id { get; set; }

        [Index(IsUnique =true)]
        public Guid Player1Token { get; set; }
        [Index(IsUnique =true)]
        public Guid Player2Token { get; set; }

        public Outcome BattleResult { get; set; }
        public DateTime Started { get; set; }
        public DateTime Finished { get; set; }

    }
}
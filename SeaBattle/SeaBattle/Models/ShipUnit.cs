﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SeaBattle.Models
{
    public class ShipUnit
    {
        [Key]
        public int Id { get; set; }

        public int ShipId { get; set; }
        public Ship Ship { get; set; }
        public byte xCoord { get; set; }
        public byte yCoord { get; set; }
        public bool Destroyed { get; set; }
    }
}
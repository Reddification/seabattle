﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SeaBattle.Models
{
    public class Ship
    {
        [Key]
        public int Id { get; set; }

        public int PlayerId { get; set; }
        public Player Player { get; set; }
        public virtual ICollection<ShipUnit> ShipUnits { get; set; }
    }
}
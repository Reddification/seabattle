﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SeaBattle.Models
{
    public class Attack
    {
        [Key]
        public int Id{ get; set; }
        public int xCoord { get; set; }
        public int yCoord { get; set; }
        public bool Missed { get; set; }

        public int PlayerId { get; set; }
        public Player Player{ get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SeaBattle.Models
{
    public class Player
    {
        [Key]
        public int Id { get; set; }

        [Required, Index(IsUnique =true),MinLength(4), MaxLength(10)]
        public string Nickname { get; set; }
        
        //persistent
        public virtual ICollection<Battle> Battles { get; set; }

        //temporary
        public virtual ICollection<Ship> Ships { get; set; }
        public virtual ICollection<Attack> Attacks {get; set; }
    }
}
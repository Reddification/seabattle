﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SeaBattle.Models
{
    public class BattleshipContext:DbContext
    {
        public BattleshipContext():base("theConnection")
        {

        }
        public DbSet<Player> Players { get; set; }
        public DbSet<Battle> Battles { get; set; }
        public DbSet<Ship> Ships { get; set; }
        public DbSet<Attack> Attacks { get; set; }
        public DbSet<ShipUnit> ShipUnits { get; set; }
    }
    
}
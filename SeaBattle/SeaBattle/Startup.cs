﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SeaBattle.Startup))]
namespace SeaBattle
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

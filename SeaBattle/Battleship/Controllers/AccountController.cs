﻿using Battleship.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using Battleship.Viewmodels;
using Microsoft.Owin.Security;
using System.Security.Claims;

namespace Battleship.Controllers
{
    public class AccountController : Controller
    {
        private AccountManager UserManager => HttpContext.GetOwinContext().GetUserManager<AccountManager>();

        private IAuthenticationManager AuthenticationManager =>HttpContext.GetOwinContext().Authentication;

        public ActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
                return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewmodel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                Account user = await UserManager.FindAsync(model.Email, model.Password);

                if (user == null)
                    ModelState.AddModelError("", "Неверный логин или пароль.");
                else
                {
                    ClaimsIdentity claim = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);
                    
                        if (string.IsNullOrEmpty(returnUrl))
                            return RedirectToAction("Index", "Home");
                    return Redirect(returnUrl);
                }
            }
            ViewBag.returnUrl = returnUrl;
            return View(model);
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Register(RegistrationViewmodel model)
        {
            if (ModelState.IsValid)
            {
                Account acc = new Account
                {
                    UserName = model.Email,
                    Email = model.Email,
                };

                IdentityResult result = await UserManager.CreateAsync(acc, model.Password, model.Nickname);

                if (result.Succeeded)
                {
                    using (BattleshipContext bs = new BattleshipContext())
                    {
                        Account newAcc = bs.Users.FirstOrDefault(a => a.Email == model.Email);
                        bs.Players.Add(new Player() { Id =newAcc.Id, Nickname = model.Nickname});
                        bs.SaveChanges();
                    }

                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
            }
            return View(model);
        }

    }
}
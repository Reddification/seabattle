﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Microsoft.Owin;
using Owin;
using Battleship.Models;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;

[assembly: OwinStartup(typeof(Battleship.App_Start.Startup))]

namespace Battleship.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // настраиваем контекст и менеджер
            app.CreatePerOwinContext<BattleshipContext>(BattleshipContext.Create);
            app.CreatePerOwinContext<AccountManager>(AccountManager.Create);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
            });                                     //вполне возможно, что по дефолту стоит

            app.MapSignalR();//, new Microsoft.AspNet.SignalR.HubConfiguration() { EnableJSONP=false});
        }
    }
}
namespace Battleship.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelTweaked : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Ships", "Player_Id", "dbo.Players");
            DropForeignKey("dbo.ShipUnits", "ShipId", "dbo.Ships");
            DropIndex("dbo.Battles", new[] { "Player1Token" });
            DropIndex("dbo.Battles", new[] { "Player2Token" });
            DropIndex("dbo.Ships", new[] { "Player_Id" });
            DropIndex("dbo.ShipUnits", new[] { "ShipId" });
            DropColumn("dbo.Ships", "PlayerId");
            RenameColumn(table: "dbo.Ships", name: "Player_Id", newName: "PlayerId");
            DropPrimaryKey("dbo.Ships");
            CreateTable(
                "dbo.CurrentBattles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BattleId = c.Int(nullable: false),
                        Player1ReconnectionToken = c.Guid(nullable: false),
                        Player2ReconnectionToken = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Battles", t => t.BattleId, cascadeDelete: true)
                .Index(t => t.BattleId)
                .Index(t => t.Player1ReconnectionToken, unique: true)
                .Index(t => t.Player2ReconnectionToken, unique: true);
            
            AddColumn("dbo.Players", "IsBot", c => c.Boolean(nullable: false));
            AddColumn("dbo.Ships", "BattleId", c => c.Int(nullable: false));
            AddColumn("dbo.ShipUnits", "PlayerId", c => c.String(maxLength: 128));
            AddColumn("dbo.ShipUnits", "BattleId", c => c.Int(nullable: false));
            AlterColumn("dbo.Ships", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Ships", "PlayerId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Ships", "PlayerId", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Ships", new[] { "Id", "PlayerId", "BattleId" });
            CreateIndex("dbo.Ships", "PlayerId");
            CreateIndex("dbo.Ships", "BattleId");
            CreateIndex("dbo.ShipUnits", new[] { "ShipId", "PlayerId", "BattleId" });
            AddForeignKey("dbo.Ships", "BattleId", "dbo.Battles", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Ships", "PlayerId", "dbo.Players", "Id", cascadeDelete: false);
            AddForeignKey("dbo.ShipUnits", new[] { "ShipId", "PlayerId", "BattleId" }, "dbo.Ships", new[] { "Id", "PlayerId", "BattleId" });
            DropColumn("dbo.Players", "AccountId");
            DropColumn("dbo.Battles", "Player1Token");
            DropColumn("dbo.Battles", "Player2Token");

            Sql(@"insert into AspNetUsers  (Id, Email, EmailConfirmed,PhoneNumberConfirmed,LockoutEnabled,AccessFailedCount,TwoFactorEnabled,UserName)
                  values (NEWID(),'serviceaccount@battleship.com', 1,1,0,0,0,'Bot');");

            Sql(@"insert into Players (Id, Nickname, IsBot)
                  select top 1 anu.Id, 'The Bot', 1 from AspNetUsers anu where anu.Email='serviceaccount@battleship.com'");
        }

        public override void Down()
        {
            Sql("delete from AspNetUsers where email='serviceaccount@battleship.com'");
            Sql("delete from Players where IsBot=1 and Nickname = 'The Bot'");


            AddColumn("dbo.Battles", "Player2Token", c => c.Guid(nullable: false));
            AddColumn("dbo.Battles", "Player1Token", c => c.Guid(nullable: false));
            AddColumn("dbo.Players", "AccountId", c => c.Int(nullable: false));
            DropForeignKey("dbo.ShipUnits", new[] { "ShipId", "PlayerId", "BattleId" }, "dbo.Ships");
            DropForeignKey("dbo.Ships", "PlayerId", "dbo.Players");
            DropForeignKey("dbo.CurrentBattles", "BattleId", "dbo.Battles");
            DropForeignKey("dbo.Ships", "BattleId", "dbo.Battles");
            DropIndex("dbo.CurrentBattles", new[] { "Player2ReconnectionToken" });
            DropIndex("dbo.CurrentBattles", new[] { "Player1ReconnectionToken" });
            DropIndex("dbo.CurrentBattles", new[] { "BattleId" });
            DropIndex("dbo.ShipUnits", new[] { "ShipId", "PlayerId", "BattleId" });
            DropIndex("dbo.Ships", new[] { "BattleId" });
            DropIndex("dbo.Ships", new[] { "PlayerId" });
            DropPrimaryKey("dbo.Ships");
            AlterColumn("dbo.Ships", "PlayerId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Ships", "PlayerId", c => c.Int(nullable: false));
            AlterColumn("dbo.Ships", "Id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.ShipUnits", "BattleId");
            DropColumn("dbo.ShipUnits", "PlayerId");
            DropColumn("dbo.Ships", "BattleId");
            DropColumn("dbo.Players", "IsBot");
            DropTable("dbo.CurrentBattles");
            AddPrimaryKey("dbo.Ships", "Id");
            RenameColumn(table: "dbo.Ships", name: "PlayerId", newName: "Player_Id");
            AddColumn("dbo.Ships", "PlayerId", c => c.Int(nullable: false));
            CreateIndex("dbo.ShipUnits", "ShipId");
            CreateIndex("dbo.Ships", "Player_Id");
            CreateIndex("dbo.Battles", "Player2Token", unique: true);
            CreateIndex("dbo.Battles", "Player1Token", unique: true);
            AddForeignKey("dbo.ShipUnits", "ShipId", "dbo.Ships", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Ships", "Player_Id", "dbo.Players", "Id");
        }
    }
}

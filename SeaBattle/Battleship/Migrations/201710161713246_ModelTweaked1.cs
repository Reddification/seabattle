namespace Battleship.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelTweaked1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Attacks", "Player_Id", "dbo.Players");
            DropForeignKey("dbo.CurrentBattles", "BattleId", "dbo.Battles");
            DropIndex("dbo.Attacks", new[] { "Player_Id" });
            DropIndex("dbo.Battles", new[] { "Player1_Id" });
            DropIndex("dbo.Battles", new[] { "Player2_Id" });
            DropIndex("dbo.CurrentBattles", new[] { "Player1ReconnectionToken" });
            DropIndex("dbo.CurrentBattles", new[] { "Player2ReconnectionToken" });
            DropColumn("dbo.Attacks", "PlayerId");
            DropColumn("dbo.Battles", "Player1Id");
            DropColumn("dbo.Battles", "Player2Id");
            RenameColumn(table: "dbo.Attacks", name: "Player_Id", newName: "PlayerId");
            RenameColumn(table: "dbo.Battles", name: "Player1_Id", newName: "Player1Id");
            RenameColumn(table: "dbo.Battles", name: "Player2_Id", newName: "Player2Id");
            DropPrimaryKey("dbo.Attacks");
            DropPrimaryKey("dbo.CurrentBattles");
            AddColumn("dbo.Attacks", "BattleId", c => c.Int(nullable: false));
            AddColumn("dbo.CurrentBattles", "Player1Token", c => c.Guid(nullable: false));
            AddColumn("dbo.CurrentBattles", "Player2Token", c => c.Guid(nullable: false));
            AlterColumn("dbo.Attacks", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Attacks", "PlayerId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Attacks", "PlayerId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Battles", "Player1Id", c => c.String(maxLength: 128));
            AlterColumn("dbo.Battles", "Player2Id", c => c.String(maxLength: 128));
            AddPrimaryKey("dbo.Attacks", new[] { "Id", "PlayerId", "BattleId" });
            AddPrimaryKey("dbo.CurrentBattles", "BattleId");
            CreateIndex("dbo.Attacks", "PlayerId");
            CreateIndex("dbo.Attacks", "BattleId");
            CreateIndex("dbo.Battles", "Player1Id");
            CreateIndex("dbo.Battles", "Player2Id");
            CreateIndex("dbo.CurrentBattles", "Player1Token", unique: true);
            CreateIndex("dbo.CurrentBattles", "Player2Token", unique: true);
            AddForeignKey("dbo.Attacks", "BattleId", "dbo.Battles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Attacks", "PlayerId", "dbo.Players", "Id", cascadeDelete: true);
            AddForeignKey("dbo.CurrentBattles", "BattleId", "dbo.Battles", "Id");
            DropColumn("dbo.CurrentBattles", "Id");
            DropColumn("dbo.CurrentBattles", "Player1ReconnectionToken");
            DropColumn("dbo.CurrentBattles", "Player2ReconnectionToken");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CurrentBattles", "Player2ReconnectionToken", c => c.Guid(nullable: false));
            AddColumn("dbo.CurrentBattles", "Player1ReconnectionToken", c => c.Guid(nullable: false));
            AddColumn("dbo.CurrentBattles", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.CurrentBattles", "BattleId", "dbo.Battles");
            DropForeignKey("dbo.Attacks", "PlayerId", "dbo.Players");
            DropForeignKey("dbo.Attacks", "BattleId", "dbo.Battles");
            DropIndex("dbo.CurrentBattles", new[] { "Player2Token" });
            DropIndex("dbo.CurrentBattles", new[] { "Player1Token" });
            DropIndex("dbo.Battles", new[] { "Player2Id" });
            DropIndex("dbo.Battles", new[] { "Player1Id" });
            DropIndex("dbo.Attacks", new[] { "BattleId" });
            DropIndex("dbo.Attacks", new[] { "PlayerId" });
            DropPrimaryKey("dbo.CurrentBattles");
            DropPrimaryKey("dbo.Attacks");
            AlterColumn("dbo.Battles", "Player2Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Battles", "Player1Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Attacks", "PlayerId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Attacks", "PlayerId", c => c.Int(nullable: false));
            AlterColumn("dbo.Attacks", "Id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.CurrentBattles", "Player2Token");
            DropColumn("dbo.CurrentBattles", "Player1Token");
            DropColumn("dbo.Attacks", "BattleId");
            AddPrimaryKey("dbo.CurrentBattles", "Id");
            AddPrimaryKey("dbo.Attacks", "Id");
            RenameColumn(table: "dbo.Battles", name: "Player2Id", newName: "Player2_Id");
            RenameColumn(table: "dbo.Battles", name: "Player1Id", newName: "Player1_Id");
            RenameColumn(table: "dbo.Attacks", name: "PlayerId", newName: "Player_Id");
            AddColumn("dbo.Battles", "Player2Id", c => c.Int(nullable: false));
            AddColumn("dbo.Battles", "Player1Id", c => c.Int(nullable: false));
            AddColumn("dbo.Attacks", "PlayerId", c => c.Int(nullable: false));
            CreateIndex("dbo.CurrentBattles", "Player2ReconnectionToken", unique: true);
            CreateIndex("dbo.CurrentBattles", "Player1ReconnectionToken", unique: true);
            CreateIndex("dbo.Battles", "Player2_Id");
            CreateIndex("dbo.Battles", "Player1_Id");
            CreateIndex("dbo.Attacks", "Player_Id");
            AddForeignKey("dbo.CurrentBattles", "BattleId", "dbo.Battles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Attacks", "Player_Id", "dbo.Players", "Id");
        }
    }
}

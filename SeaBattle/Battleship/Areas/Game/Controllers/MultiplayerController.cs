﻿using Battleship.Areas.Game.Models;
using Battleship.Hubs;
using Battleship.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Battleship.Areas.Game.Controllers
{
    public class MultiplayerController : Controller
    {
        Battleship.Models.BattleshipContext dbContext = new Battleship.Models.BattleshipContext();

        private readonly static Lazy<MultiplayerController> LobbyHub = new Lazy<MultiplayerController>(() => 
        new MultiplayerController(GlobalHost.ConnectionManager.GetHubContext<OnlinePlayersHub>()));

        IHubContext hubContext;
        public MultiplayerController()
        {}

        public MultiplayerController(IHubContext ihc)
        {
            this.hubContext = ihc;
        }

        [System.Web.Mvc.Authorize]
        public ActionResult Lobby()
        {
            return View(dbContext);
        }

        [HttpPost]
        public ActionResult Attack(byte xCoord, byte yCoord, int battleId, Guid token)
        {
            Player attackingPlayer = null, targetPlayer=null;
            CurrentBattle currentBattle = dbContext.CurrentBattles.FirstOrDefault(bs => bs.BattleId == battleId);

            if (currentBattle==null)
                return Json(new { Success=false, Message="Такой игры не существует", HttpCode=HttpStatusCode.NotFound});

            if (currentBattle.Player1Token == token)
            {
                attackingPlayer = currentBattle.Battle.Player1;
                targetPlayer = currentBattle.Battle.Player2;
            }
            else if (currentBattle.Player2Token == token)
            {
                attackingPlayer = currentBattle.Battle.Player2;
                targetPlayer = currentBattle.Battle.Player1;
            }
            else return Json(new { Success = false, Message = "Игрок не участвует в этом бою", HttpCode = HttpStatusCode.Forbidden });

            Attack a = new Battleship.Models.Attack() { xCoord = xCoord, yCoord = yCoord };
            ShipUnit wreckedUnit = currentBattle.Battle.Ships.Where(ship => ship.PlayerId == targetPlayer.Id)
                                                .SelectMany(s => s.ShipUnits)
                                                .FirstOrDefault(su => su.xCoord == xCoord && su.yCoord == yCoord && !su.Destroyed);
            if (wreckedUnit != null)
            {
                a.Missed = false;
                wreckedUnit.Destroyed = true;
            }
            else a.Missed = true;

            //TODO: не представляю, как эта хуйня поведет себя, ведь она зависит от боя и игрока
            currentBattle.Battle.Attacks.Add(a);

            if (currentBattle.Battle.Ships.Where(s => s.PlayerId == targetPlayer.Id).SelectMany(s => s.ShipUnits).All(su => su.Destroyed))
                Gameover(dbContext, currentBattle, targetPlayer.Id == currentBattle.Battle.Player1Id ? Outcome.Player2Won : Outcome.Player1Won);
            
            //signal R about the hit
            dbContext.SaveChangesAsync();
            return Json(new { Success = true, HttpCode = HttpStatusCode.OK });
        }
        private void Gameover(BattleshipContext context, CurrentBattle battle, Outcome otc)
        {
            battle.Battle.Finished = DateTime.Now;
            battle.Battle.BattleResult = otc;

            throw new NotImplementedException("Signal R shit");

            context.CurrentBattles.Remove(battle);
            context.Attacks.RemoveRange(battle.Battle.Attacks);
            context.ShipUnits.RemoveRange(battle.Battle.Ships.SelectMany(s => s.ShipUnits));
            context.Ships.RemoveRange(battle.Battle.Ships);
            context.CurrentBattles.Remove(battle);
        }
        
        [HttpPost]
        public ActionResult InitiateBattle(string PlayerId)
        {
            throw new NotImplementedException("SignalR shit");
        }

        [HttpPost]
        public ActionResult FindBattle()
        {
            throw new NotImplementedException("SignalR shit");
        }

        [HttpPost]
        public ActionResult AcceptBattle()
        {
            throw new NotImplementedException("SignalR shit");
        }

        [HttpPost]
        public ActionResult DeclineBattle()
        {
            throw new NotImplementedException("SignalR shit");
        }

        [HttpPost]
        public ActionResult QuitGame()
        {
            throw new NotImplementedException("SignalR, opponent status checks shit");
        }
    }
}
﻿using Battleship.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Battleship.Areas.Game.AI
{
    public abstract class ArtificialIntelligence
    {
        protected abstract Ship AllocateShips();
        protected abstract Attack Attack();
    }
}
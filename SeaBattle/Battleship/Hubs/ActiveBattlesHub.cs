﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Battleship.Models;
using System.Threading.Tasks;
using System.Net;

namespace Battleship.Hubs
{
    public class ActiveBattlesHub : Hub
    {
        public void Attack(byte xCoord, byte yCoord, int battleId, Guid token)
        {
            using (BattleshipContext bc = new BattleshipContext())
            {
                Player attackingPlayer = null, targetPlayer = null;
                CurrentBattle currentBattle = bc.CurrentBattles.FirstOrDefault(bs => bs.BattleId == battleId);

                if (currentBattle == null)
                {
                    Clients.Caller.hitResult(new { Success = false, Message = "Такой игры не существует", HttpCode = HttpStatusCode.NotFound });
                    return;
                }

                if (currentBattle.Player1Token == token)
                {
                    attackingPlayer = currentBattle.Battle.Player1;
                    targetPlayer = currentBattle.Battle.Player2;
                }
                else if (currentBattle.Player2Token == token)
                {
                    attackingPlayer = currentBattle.Battle.Player2;
                    targetPlayer = currentBattle.Battle.Player1;
                }
                else
                {
                    Clients.Caller.hitResult(new { Success = false, Message = "Игрок не участвует в этом бою", HttpCode = HttpStatusCode.Forbidden });
                    return;
                }
                Attack a = new Battleship.Models.Attack() { xCoord = xCoord, yCoord = yCoord };
                ShipUnit wreckedUnit = currentBattle.Battle.Ships.Where(ship => ship.PlayerId == targetPlayer.Id)
                                                    .SelectMany(s => s.ShipUnits)
                                                    .FirstOrDefault(su => su.xCoord == xCoord && su.yCoord == yCoord && !su.Destroyed);
                if (wreckedUnit != null)
                    wreckedUnit.Destroyed = true;
                else a.Missed = true;

                //TODO: не представляю, как эта хуйня поведет себя, ведь она зависит от боя и игрока
                currentBattle.Battle.Attacks.Add(a);

                if (currentBattle.Battle.Ships.Where(s => s.PlayerId == targetPlayer.Id).SelectMany(s => s.ShipUnits).All(su => su.Destroyed))
                    Gameover(bc, currentBattle, targetPlayer.Id == currentBattle.Battle.Player1Id ? Outcome.Player2Won : Outcome.Player1Won);

                //signal R about the hit
                bc.SaveChangesAsync();
                Clients.Caller.hitResult(new { Success = true, HttpCode = HttpStatusCode.OK });
            }
        }

        private void Gameover(BattleshipContext context, CurrentBattle battle, Outcome otc)
        {
            using (BattleshipContext dbContext=new BattleshipContext())
            {
                battle.Battle.Finished = DateTime.Now;
                battle.Battle.BattleResult = otc;

                Clients.Group(battle.BattleId.ToString()).gameover(otc.ToString());
                Groups.Remove(Context.ConnectionId, battle.BattleId.ToString());
                //Groups.Remove(dbContext.OnlinePlayers.Single(s => s.ConnectionId != Context.ConnectionId && (battle.Battle.Player1Id == s.PlayerId || battle.Battle.Player2Id == s.Id)).ConnectionId, battle.BattleId.ToString());

                context.CurrentBattles.Remove(battle);
                context.Attacks.RemoveRange(battle.Battle.Attacks);
                context.ShipUnits.RemoveRange(battle.Battle.Ships.SelectMany(s => s.ShipUnits));
                context.Ships.RemoveRange(battle.Battle.Ships);
                context.CurrentBattles.Remove(battle);
            } 
        }

    }
}
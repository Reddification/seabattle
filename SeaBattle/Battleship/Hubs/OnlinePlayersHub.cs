﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using Battleship.Models;
using Newtonsoft.Json;
using Battleship.Areas.Game.Models;
using System.Net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR.Hubs;

namespace Battleship.Hubs
{
    public enum @event
    {
        BattleFinished=1,
        BattleStarted=2,
        OpponentDisconnected=3,
        OpponentLeft=4,
    }
    public class BattleEvent
    {
        public @event e { get; set; }
        public string Message { get; set; }
    }
    

    [HubName("onlinePlayersLobby")]
    public class OnlinePlayersHub : Hub
    {
        public override Task OnConnected()
        {
            //string playerId = HttpContext.Current.User.Identity.GetUserId();
            ////playerId = Context.Request.GetHttpContext().User.Identity.GetUserId<string>();
        
            //using (BattleshipContext bc = new BattleshipContext())
            //{
            //    Player p = bc.Players.Find(playerId);
            //    OnlinePlayersSingleton.Players.Add(new LobbyPlayer
            //    {
            //        Id=playerId,
            //        ConnectionId=Context.ConnectionId,
            //        Nickname=p.Nickname,
            //        Status=PlayerStatus.Online,
            //        //WinRate=
            //    });
            //}
            return base.OnConnected();
        }

        public override Task OnReconnected()
        {
            return base.OnReconnected();
        }

        public void SendMessage(string message)
        {
            using (BattleshipContext dbContext = new BattleshipContext())
            {
                string Nickname = dbContext.OnlinePlayers.FirstOrDefault(lp => lp.ConnectionId == Context.ConnectionId).Player.Nickname;
                Clients.All.AddMessage(Nickname ?? "Анонимус", message);
            }
        }

        public void Connect()
        {
            using (BattleshipContext BC = new BattleshipContext())
            {
                string connectionId = Context.ConnectionId;

                OnlinePlayer onlinePlayer = null;

                if ((onlinePlayer = BC.OnlinePlayers.FirstOrDefault(x => x.ConnectionId == connectionId))==null)
                {
                    string playerId = Context.User.Identity.GetUserId<string>();
                    Player p =  BC.Players.FirstOrDefault(u=>u.Id== playerId);
                    
                    #region experimental
                    //float winrate=0;
                    //var playerBattles = BC.Battles.Where(b => b.Player1Id == p.Id || b.Player2Id == p.Id);

                    //if (playerBattles.Count() > 0)
                    //    winrate = (float)playerBattles.Count() / playerBattles.Where(pb => pb.Player1Id == p.Id && pb.BattleResult == Outcome.Player1Won || pb.Player2Id == p.Id && pb.BattleResult == Outcome.Player2Won).Count();
                    #endregion

                    if (p != null)
                    {
                        onlinePlayer = new OnlinePlayer()
                        {
                            ConnectionId = connectionId,
                            PlayerId = p.Id,
                            Status=PlayerStatus.Online
                            //WinRate = winrate
                        };
                    }
                }

                if (onlinePlayer == null)
                    return;

                BC.OnlinePlayers.Add(onlinePlayer);

                Clients.Caller.onConnected(connectionId, onlinePlayer.Player.Nickname, BC.OnlinePlayers);

                Clients.AllExcept(connectionId).onUserConnected(connectionId, onlinePlayer.Player.Nickname);

                BC.SaveChanges();
            }
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            using (BattleshipContext dbContext = new BattleshipContext())
            {
                var lPlayer = dbContext.OnlinePlayers.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);

                if (lPlayer != null)
                {
                    dbContext.OnlinePlayers.Remove(lPlayer);
                    var id = Context.ConnectionId;
                    Clients.All.onUserDisconnected(id, lPlayer.Player.Nickname);
                }
                dbContext.SaveChanges();
            }
            return base.OnDisconnected(stopCalled);
        }

        public void EnteredBattle(string BattleId)
        {
            using (BattleshipContext bc = new BattleshipContext())
            {
                OnlinePlayer onlinePlayer = bc.OnlinePlayers.FirstOrDefault(op => op.ConnectionId == Context.ConnectionId);

                this.Groups.Add(Context.ConnectionId, BattleId.ToString());

                Clients.All.StatusChanged(onlinePlayer.ConnectionId, PlayerStatus.InBattle.ToString());
            }
        }
        public void InitiateBattle(string targetUserConnectionId)
        {
            Clients.Client(targetUserConnectionId).onBattleRequested(Context.ConnectionId);
        }
        public void CancelRequest(string targetUserConnectionId)
        {
            Clients.Client(targetUserConnectionId).onBattleRequestAborted();
        }

        
        public void ResponseBattleRequest(string initiatorUserConnectionId, bool? response)
        {
            string responseDescription = 
                   response.HasValue ? 
                                    (response.Value ? "Пользователь согласился бить тобi ебало"
                                                    :"Пользователь очкошник") 
                                  : "Пользователь проигнорировал ваше предложение";
            Clients.Client(initiatorUserConnectionId).opponentResponded(response??false, responseDescription);

        }

        
    }
}
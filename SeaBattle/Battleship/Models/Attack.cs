﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Battleship.Models
{
    public class Attack
    {
        [Key, Column(Order = 0)]
        public int Id{ get; set; }
        public int xCoord { get; set; }
        public int yCoord { get; set; }
        public bool Missed { get; set; }

        [Key, ForeignKey("Player"), Column(Order = 1)]
        public string PlayerId { get; set; }
        public Player Player{ get; set; }

        [Key, ForeignKey("Battle"), Column(Order = 2)]
        public int BattleId { get; set; }
        public Battle Battle { get; set; }
    }
}
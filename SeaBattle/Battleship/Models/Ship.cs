﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Battleship.Models
{
    public class Ship
    {
        [Key, Column(Order = 0)]
        public int Id { get; set; }

        [Key, Column(Order = 2) ForeignKey("Battle")]
        public int BattleId { get; set; }
        public Battle Battle { get; set; }

        [Key, Column(Order = 1) ForeignKey("Player")]
        public string PlayerId { get; set; }
        public Player Player { get; set; }

        public virtual ICollection<ShipUnit> ShipUnits { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Battleship.Models
{
    public class Player
    {
        [Key, ForeignKey("Account")]
        public string Id { get; set; }
        public string Nickname { get; set; }

        //persistent
        public virtual ICollection<Battle> Battles { get; set; }

        //temporary
        //public virtual ICollection<Ship> Ships { get; set; }
        //public virtual ICollection<Attack> Attacks { get; set; }
        public bool IsBot { get; set; }
        public virtual Account Account { get; set; }
    }
}
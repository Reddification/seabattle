﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Battleship.Models
{
    public class ShipUnit
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Ship"), Column(Order =0)]
        public int ShipId { get; set; }

        [ForeignKey("Player"), Column(Order = 1)]
        public string PlayerId { get; set; }

        [ForeignKey("Battle"), Column(Order = 2)]
        public int BattleId { get; set; }

        public Ship Ship { get; set; }
        public Player Player { get; set; }
        public Battle Battle { get; set; }

        public byte xCoord { get; set; }
        public byte yCoord { get; set; }
        public bool Destroyed { get; set; }
    }
}
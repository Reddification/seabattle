﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Battleship.Models
{
    public enum PlayerStatus
    {
        Online = 1,
        Offline=2,
        AFK = 3,
        InBattle = 4,
    }

    public class OnlinePlayer
    {
        [Key]
        public string ConnectionId { get; set; }

        [ForeignKey("Player"), Index(IsUnique = true)]
        public string PlayerId { get; set; }
        public Player Player { get; set; }

        public PlayerStatus Status { get; set; }
    }
}
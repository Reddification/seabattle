﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Battleship.Models
{
    public enum Outcome { Player1Won=1, Player2Won=2, Player1Disconnected=3, Player2Disconnected=4}

    public class Battle
    {
        [Key]
        public int Id { get; set; }

        public string Player1Id { get; set; }
        public string Player2Id { get; set; }

        [ForeignKey("Player1Id")]
        public Player Player1 { get; set; }
        [ForeignKey("Player2Id")]
        public Player Player2 { get; set; }


        public Outcome BattleResult { get; set; }

        public virtual ICollection<Ship> Ships { get; set; }
        public virtual ICollection<Attack> Attacks { get; set; }
        public DateTime Started { get; set; }
        public DateTime Finished { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Battleship.Models
{
    public enum BattleStatus
    {
        DislocatingShips=1,
        InAction=2,
        Finished=3
    }
    public class CurrentBattle
    {
        [ForeignKey("Battle"), Index(IsUnique =true)]
        public int BattleId { get; set; }

        public Battle Battle { get; set; }

        [Index(IsUnique =true)]
        public Guid Player1Token { get; set; }
        [Index(IsUnique =true)]
        public Guid Player2Token { get; set; }
        public BattleStatus Status { get; set; }
    }
}
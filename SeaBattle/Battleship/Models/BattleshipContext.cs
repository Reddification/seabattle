﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Battleship.Models
{
    public class BattleshipContext: IdentityDbContext<Account>
    {
        public BattleshipContext():base("Battleshit")
        {

        }
        public DbSet<Player> Players { get; set; }
        public DbSet<Battle> Battles { get; set; }
        public DbSet<Ship> Ships { get; set; }
        public DbSet<Attack> Attacks { get; set; }
        public DbSet<ShipUnit> ShipUnits { get; set; }

        //непостоянные таблицы
        public virtual DbSet<CurrentBattle> CurrentBattles { get; set; }
        public virtual DbSet<OnlinePlayer> OnlinePlayers { get; set; }

        public DbSet<Account> Accounts { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);

        //    //modelBuilder.Entity<Account>()
        //    //       .HasRequired(s => s.Player)
        //    //       .WithMany()
        //    //       .HasForeignKey(s => s.PlayerId);

        //    //modelBuilder.Entity<Player>()
        //    //            .HasOptional(s => s.Account)
        //    //            .WithMany()
        //    //            .HasForeignKey(s => s.AccountId);
        //}
        public static BattleshipContext Create() => new BattleshipContext();
    }
}
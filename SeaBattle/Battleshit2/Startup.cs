﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Battleshit2.Startup))]
namespace Battleshit2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

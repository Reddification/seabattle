﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database
{
    class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Username { get; set; }
        public bool Destroyed { get; set; }
    }
}

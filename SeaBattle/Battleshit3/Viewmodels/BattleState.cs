﻿using Battleshit3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Battleshit3.Viewmodels
{
    public class BattleState
    {
        public List<Ship> Ships { get; set; }
        public List<Attack> Attacks { get; set; }
    }

}
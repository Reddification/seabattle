﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Battleshit3.Viewmodels
{
    public class ActiveBattle
    {
        public int BattleId { get; set; }
        public string OpponentNickname { get; set; }
        public DateTime Started { get; set; }
    }
}
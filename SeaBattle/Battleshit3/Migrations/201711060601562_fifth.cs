namespace Battleshit3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fifth : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Battles", new[] { "BattleToken" });
            DropIndex("dbo.PendingBattles", new[] { "PendingBattleToken" });
            AlterColumn("dbo.Battles", "BattleToken", c => c.Guid(nullable: false));
            AlterColumn("dbo.PendingBattles", "PendingBattleToken", c => c.Guid(nullable: false));
            CreateIndex("dbo.Battles", "BattleToken", unique: true);
            CreateIndex("dbo.PendingBattles", "PendingBattleToken", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.PendingBattles", new[] { "PendingBattleToken" });
            DropIndex("dbo.Battles", new[] { "BattleToken" });
            AlterColumn("dbo.PendingBattles", "PendingBattleToken", c => c.Guid(nullable: false));
            AlterColumn("dbo.Battles", "BattleToken", c => c.Guid(nullable: false));
            CreateIndex("dbo.PendingBattles", "PendingBattleToken", unique: true);
            CreateIndex("dbo.Battles", "BattleToken", unique: true);
        }
    }
}

namespace Battleshit3.Migrations
{
    using Properties;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Battleshit3.Models.BattleshipContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Battleshit3.Models.BattleshipContext context)
        {

            context.Users.AddOrUpdate(u=>u.Email,
                new Models.Account()
                {
                
                    Email = Settings.Default.ServiceAccountEmail,
                    UserName = "bot",
                    EmailConfirmed = true,
                    LockoutEnabled=true,
                });
            context.SaveChanges();
            //context.Players.AddOrUpdate(p => p.Id, 
            //    new Models.Player()
            //    {
            //        Id = context.Users.FirstOrDefault(u => u.Email == Settings.Default.ServiceAccountEmail).Id,
            //        IsBot = true,
            //        Nickname = "the bot"
            //    });
            //context.SaveChanges();
        }
    }
}

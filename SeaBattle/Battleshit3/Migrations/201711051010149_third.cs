namespace Battleshit3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class third : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PlayerSessions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PlayerId = c.String(nullable: false, maxLength: 128),
                        ConnectionId = c.String(),
                        Started = c.DateTime(nullable: false),
                        Finished = c.DateTime(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId);
            
            DropColumn("dbo.Players", "ConnectionId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Players", "ConnectionId", c => c.String());
            DropForeignKey("dbo.PlayerSessions", "PlayerId", "dbo.Players");
            DropIndex("dbo.PlayerSessions", new[] { "PlayerId" });
            DropTable("dbo.PlayerSessions");
        }
    }
}

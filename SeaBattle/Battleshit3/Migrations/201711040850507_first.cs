namespace Battleshit3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attacks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        xCoord = c.Byte(nullable: false),
                        yCoord = c.Byte(nullable: false),
                        HitResult = c.Byte(nullable: false),
                        PlayerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.PlayerId)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "dbo.Players",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        ConnectionId = c.String(),
                        Nickname = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Ships",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PlayerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.PlayerId)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "dbo.ShipUnits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ShipId = c.Int(nullable: false),
                        xCoord = c.Byte(nullable: false),
                        yCoord = c.Byte(nullable: false),
                        Destroyed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ships", t => t.ShipId, cascadeDelete: true)
                .Index(t => t.ShipId);
            
            CreateTable(
                "dbo.Battles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BattleToken = c.Guid(nullable: false),
                        Player1Id = c.String(maxLength: 128),
                        Player2Id = c.String(maxLength: 128),
                        BattleResult = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Mode = c.Int(nullable: false),
                        Started = c.DateTime(nullable: false),
                        Finished = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.Player1Id)
                .ForeignKey("dbo.Players", t => t.Player2Id)
                .Index(t => t.BattleToken, unique: true)
                .Index(t => t.Player1Id)
                .Index(t => t.Player2Id);
            
            CreateTable(
                "dbo.PendingBattles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PendingBattleToken = c.Guid(nullable: false),
                        Player1Id = c.String(maxLength: 128),
                        Player2Id = c.String(maxLength: 128),
                        Player1Status = c.Byte(nullable: false),
                        Player2Status = c.Byte(nullable: false),
                        Closed = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.Player1Id)
                .ForeignKey("dbo.Players", t => t.Player2Id)
                .Index(t => t.PendingBattleToken, unique: true)
                .Index(t => t.Player1Id)
                .Index(t => t.Player2Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.WaitingPlayers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PlayerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.PlayerId)
                .Index(t => t.PlayerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WaitingPlayers", "PlayerId", "dbo.Players");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.PendingBattles", "Player2Id", "dbo.Players");
            DropForeignKey("dbo.PendingBattles", "Player1Id", "dbo.Players");
            DropForeignKey("dbo.Battles", "Player2Id", "dbo.Players");
            DropForeignKey("dbo.Battles", "Player1Id", "dbo.Players");
            DropForeignKey("dbo.ShipUnits", "ShipId", "dbo.Ships");
            DropForeignKey("dbo.Ships", "PlayerId", "dbo.Players");
            DropForeignKey("dbo.Attacks", "PlayerId", "dbo.Players");
            DropForeignKey("dbo.Players", "Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.WaitingPlayers", new[] { "PlayerId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.PendingBattles", new[] { "Player2Id" });
            DropIndex("dbo.PendingBattles", new[] { "Player1Id" });
            DropIndex("dbo.PendingBattles", new[] { "PendingBattleToken" });
            DropIndex("dbo.Battles", new[] { "Player2Id" });
            DropIndex("dbo.Battles", new[] { "Player1Id" });
            DropIndex("dbo.Battles", new[] { "BattleToken" });
            DropIndex("dbo.ShipUnits", new[] { "ShipId" });
            DropIndex("dbo.Ships", new[] { "PlayerId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Players", new[] { "Id" });
            DropIndex("dbo.Attacks", new[] { "PlayerId" });
            DropTable("dbo.WaitingPlayers");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.PendingBattles");
            DropTable("dbo.Battles");
            DropTable("dbo.ShipUnits");
            DropTable("dbo.Ships");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Players");
            DropTable("dbo.Attacks");
        }
    }
}

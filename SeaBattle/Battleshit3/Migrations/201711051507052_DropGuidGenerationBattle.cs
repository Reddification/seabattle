namespace Battleshit3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropGuidGenerationBattle : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Battles", new[] { "BattleToken" });
            AlterColumn("dbo.Battles", "BattleToken", c => c.Guid(nullable: false));
            CreateIndex("dbo.Battles", "BattleToken", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Battles", new[] { "BattleToken" });
            AlterColumn("dbo.Battles", "BattleToken", c => c.Guid(nullable: false));
            CreateIndex("dbo.Battles", "BattleToken", unique: true);
        }
    }
}

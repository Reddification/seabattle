namespace Battleshit3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropGuidGenerationPendingBattle : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PendingBattles", new[] { "PendingBattleToken" });
            AlterColumn("dbo.PendingBattles", "PendingBattleToken", c => c.Guid(nullable: false));
            CreateIndex("dbo.PendingBattles", "PendingBattleToken", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.PendingBattles", new[] { "PendingBattleToken" });
            AlterColumn("dbo.PendingBattles", "PendingBattleToken", c => c.Guid(nullable: false));
            CreateIndex("dbo.PendingBattles", "PendingBattleToken", unique: true);
        }
    }
}

namespace Battleshit3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fourth : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Attacks", "xCoord", c => c.Short(nullable: false));
            AlterColumn("dbo.Attacks", "yCoord", c => c.Short(nullable: false));
            AlterColumn("dbo.ShipUnits", "xCoord", c => c.Short(nullable: false));
            AlterColumn("dbo.ShipUnits", "yCoord", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ShipUnits", "yCoord", c => c.Byte(nullable: false));
            AlterColumn("dbo.ShipUnits", "xCoord", c => c.Byte(nullable: false));
            AlterColumn("dbo.Attacks", "yCoord", c => c.Byte(nullable: false));
            AlterColumn("dbo.Attacks", "xCoord", c => c.Byte(nullable: false));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Battleshit3.Models
{
    public enum PlayerOfferStatus :byte
    {
        //Waiting - это когда модальное окно "предлагаем вам сразиться с таким то оппонентом"
        Waiting =0,
        Accepted=1,
        Declined=2,
        //хз, скорее всего, избыточное. не знаю, как и куда пока прикрутить, но концептуально оппонент вполне может морозиться и не отвечать на вызов
        Ignored=3,
        Ready=4,
        Canceled = 5
    }

    public class PendingBattle
    {
        [Key]
        public int Id { get; set; }

        [Index(IsUnique =true)]
        public Guid PendingBattleToken { get; set; }
        public string Player1Id { get; set; }
        public string Player2Id { get; set; }

        [ForeignKey("Player1Id")]
        public virtual Player Player1 { get; set; }
        [ForeignKey("Player2Id")]
        public virtual Player Player2 { get; set; }

        public PlayerOfferStatus Player1Status { get; set; }
        public PlayerOfferStatus Player2Status { get; set; }

        [NotMapped]
        public bool IsActive
        {
            get
            {
                return Closed == null
                        &&     ((Player1Status != PlayerOfferStatus.Declined || Player1Status == PlayerOfferStatus.Ignored)
                            && (Player2Status != PlayerOfferStatus.Declined || Player2Status != PlayerOfferStatus.Ignored));
            }
        }

        public DateTime? Closed { get; set; }
    }
}
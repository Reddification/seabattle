﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Battleshit3.Models
{
    public class ShipUnit
    {
        [Key]
        public int Id { get; set; }

        public int ShipId { get; set; }
        [ForeignKey("ShipId")]
        public virtual Ship Ship { get; set; }

        public short xCoord { get; set; }
        public short yCoord { get; set; }
        public bool Destroyed { get; set; }
    }
}
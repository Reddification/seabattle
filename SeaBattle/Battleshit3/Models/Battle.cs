﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Battleshit3.Models
{
    public enum BattleOutcome
    {
        Player1Won = 1,
        Player2Won = 2,
        Player1Disconnected = 3,
        Player2Disconnected = 4
    }
    public enum BattleStatus
    {
        InAction = 1,
        Finished = 2,
    }
    public enum BattleMode
    {
        vsCPU=1,
        vsPlayer=2
    }
    public class Battle
    {
        [Key]
        public int Id { get; set; }

        [Index(IsUnique = true)]
        public Guid BattleToken { get; set; }

        public string Player1Id { get; set; }
        public string Player2Id { get; set; }

        [ForeignKey("Player1Id")]
        public virtual Player Player1 { get; set; }
        [ForeignKey("Player2Id")]
        public virtual Player Player2 { get; set; }

        public BattleOutcome BattleResult { get; set; }
        public BattleStatus Status { get; set; }
        public BattleMode Mode { get; set; }
        public DateTime Started { get; set; }
        public DateTime? Finished { get; set; }

    }
}
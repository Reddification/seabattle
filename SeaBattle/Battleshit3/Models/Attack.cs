﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Battleshit3.Models
{
    public enum HitResult:byte
    {
        Hit=1,
        Missed=2,
        Destroyed=3
    }

    public class Attack
    {
        [Key, Column(Order = 0)]
        public int Id{ get; set; }
        public short xCoord { get; set; }
        public short yCoord { get; set; }
        public HitResult HitResult { get; set; }

        public string PlayerId { get; set; }

        [ForeignKey("PlayerId")]
        public virtual Player Admiral{ get; set; }
    }
}
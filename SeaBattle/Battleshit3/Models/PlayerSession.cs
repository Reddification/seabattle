﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Battleshit3.Models
{
    public enum PlayerStatus { Offline=0, Online=1, Negotiating=2, InBattle=3 }
    public static class PlayerStatusExtensions
    {
        public static string GetDescription(this PlayerStatus status)
        {
            return Enum.GetName(typeof(PlayerStatus), status);
        }
    }
    public class PlayerSession:IEqualityComparer<PlayerSession>
    {
        [Key]
        public int Id { get; set; }

        [Required, ForeignKey("Player")]
        public string PlayerId { get; set; }

        public string ConnectionId { get; set; }

        public DateTime Started { get; set; }
        public DateTime? Finished { get; set; }
        public PlayerStatus Status { get; set; }
        public virtual Player Player { get; set; }

        internal void Close()
        {
            this.Finished = DateTime.Now;
            this.Status = PlayerStatus.Offline;
        }

        public bool Equals(PlayerSession x, PlayerSession y)
        {
            return x.PlayerId == y.PlayerId;
        }

        public int GetHashCode(PlayerSession obj)
        {
            return obj.Id;
        }
    }
    public class PlayerSessionDistiguisher : IEqualityComparer<PlayerSession>
    {
        public bool Equals(PlayerSession x, PlayerSession y)
        {
            return x.PlayerId == y.PlayerId;
        }

        public int GetHashCode(PlayerSession obj)
        {
            return obj.Id;
        }
    }
}
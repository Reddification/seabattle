﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace Battleshit3.Models
{
    public class AccountManager : UserManager<Account>
    {
        public AccountManager(IUserStore<Account> store) : base(store)
        {

        }

        public static AccountManager Create(IdentityFactoryOptions<AccountManager> options, IOwinContext context)
        {
            BattleshipContext db = context.Get<BattleshipContext>();
            AccountManager manager = new AccountManager(new UserStore<Account>(db));
            return manager;
        }
        public Task<IdentityResult> CreateAsync(Account user, string password, string Nickname)
        {
            #region experimental
            using (BattleshipContext db = new BattleshipContext())
            {
                if (db.Users.Any(a => a.Email == user.Email))
                    return Task<IdentityResult>.Run(() =>
                    {
                        return new IdentityResult(new string[] { "Пользователь с такой почтой уже зарегистрирован" });
                    });
                if (db.Players.Any(p => p.Nickname == Nickname))
                    return Task<IdentityResult>.Run(() =>
                    {
                        return new IdentityResult(new string[] { "Пользователь с таким ником зарегистрирован" });
                    });
            }
            return base.CreateAsync(user, password);
            #endregion
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Battleshit3.Models
{
    public class Ship
    {
        [Key, Column(Order = 0)]
        public int Id { get; set; }
        
        
        public string PlayerId { get; set; }

        [ForeignKey("PlayerId")]
        public virtual Player Admiral { get; set; }

        public virtual ICollection<ShipUnit> ShipUnits { get; set; }
    }
}
﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Battleshit3.Models
{
    public class BattleshipContext: IdentityDbContext<Account>
    {
        public BattleshipContext():base("theContext")
        {
        }
        public DbSet<Player> Players { get; set; }
        public DbSet<Battle> Battles { get; set; }
        public DbSet<PendingBattle> PendingBattles { get; set; }
        public DbSet<Ship> Ships { get; set; }
        public DbSet<Attack> Attacks { get; set; }
        public DbSet<ShipUnit> ShipUnits { get; set; }
        public DbSet<WaitingPlayer> WaitingPlayers { get; set; }
        public DbSet<PlayerSession> PlayerSessions { get; set; }
        //public DbSet<Account> Accounts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<BattleshipContext, Configuration>());
            base.OnModelCreating(modelBuilder);
        }
        public static BattleshipContext Create() => new BattleshipContext();

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Battleshit3.Models
{
    public class Player
    {
        [Key, ForeignKey("Account")]
        public string Id { get; set; }
        public string Nickname { get; set; }
        public bool IsBot { get; set; }
        public virtual ICollection<Ship> Ships { get; set; }
        public virtual ICollection<Attack> Attacks { get; set; }
        public virtual ICollection<PlayerSession> PlayerSessions { get; set; }
        public virtual Account Account { get; set; }

        [NotMapped]
        internal string CurrentConnectionId=>this.PlayerSessions.Where(l => l.Finished == null).OrderBy(l=>l.Started).Last().ConnectionId;

        [NotMapped]
        internal PlayerSession CurrentSession => this.PlayerSessions.Where(s => s.Finished == null).OrderBy(s => s.Started).Last();
    }
}
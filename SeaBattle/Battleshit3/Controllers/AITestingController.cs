﻿using Battleshit3.AI;
using Battleshit3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Battleshit3.Controllers
{
    public class AITestingController : Controller
    {
        // GET: AITesting
        public ActionResult Index()
        {
            List<Ship> targetShips = ArtificialIntelligence.DislocateShips().ToList();
            List<Attack> Attacks = new List<Attack>();

            int counter = 0;
            while (!targetShips.All(s => s.ShipUnits.All(su => su.Destroyed)))
            {
                Attack a = ArtificialIntelligence.GenerateAttack(Attacks);
                ShipUnit destroyedShipUnit = targetShips.SelectMany(s => s.ShipUnits).FirstOrDefault(su => su.xCoord == a.xCoord && su.yCoord == a.yCoord && !su.Destroyed);

                if (destroyedShipUnit != null)
                {
                    if (targetShips.First(s => s.ShipUnits.Any(su => su.xCoord == destroyedShipUnit.xCoord && su.yCoord == destroyedShipUnit.yCoord)).ShipUnits.All(su => su.Destroyed))
                        a.HitResult = HitResult.Destroyed;
                    else a.HitResult = HitResult.Hit;
                }
                else a.HitResult = HitResult.Missed;

                Attacks.Add(a);

                counter++;
            }

            return View();
        }
    }
}
﻿using Battleshit3.AI;
using Battleshit3.Hubs;
using Battleshit3.Models;
using Battleshit3.Properties;
using Battleshit3.Viewmodels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Battleshit3.Controllers
{
    /*
     Последовательность:
     onConnected

         */

    public class BattleController : Controller
    {


        BattleshipContext dbContext = new BattleshipContext();
        private const string battleCookie = "battleToken";
        private const string pendingBattleCookie = "pendingBattleToken";

        IHubContext hub = GlobalHost.ConnectionManager.GetHubContext<BattleshipHub>();

        [System.Web.Mvc.Authorize]
        public async Task<ActionResult> Lobby()
        {
            return View("Lobby", null);
        }

        [HttpPost]
        public void DropThatShit()
        {
            using (BattleshipContext dbContext = new BattleshipContext())
            {
                dbContext.Database.ExecuteSqlCommand("delete from [PendingBattles]");
                dbContext.Database.ExecuteSqlCommand("delete from [Battles]");
                dbContext.Database.ExecuteSqlCommand("delete from [WaitingPlayers]");
                dbContext.Database.ExecuteSqlCommand("delete from [Attacks]");
                dbContext.Database.ExecuteSqlCommand("delete from [ShipUnits]");
                dbContext.Database.ExecuteSqlCommand("delete from [Ships]");
                dbContext.Database.ExecuteSqlCommand("delete from [Players]");
                dbContext.SaveChanges();
            }
        }

        [HttpPost]
        public async Task Attack(short xCoord, short yCoord)
        {
            Player thisPlayer = initializePlayer();
            
            Battle currentBattle = DetermineBattle();//dbContext.Battles.FirstOrDefault(b => b.Player1Id == thisPlayer.Id || b.Player2Id == thisPlayer.Id);

            if (currentBattle == null || currentBattle.Status == BattleStatus.Finished)
            {
                hub.Clients.User(thisPlayer.Id).onError(403, "Такой игры не существует");
                return;
            }

            Player targetPlayer = currentBattle.Player1Id == thisPlayer.Id ? currentBattle.Player2 : currentBattle.Player1;
            string targetPlayerConnectionId = targetPlayer.PlayerSessions.Last(ps => ps.Finished == null).ConnectionId;

            Attack thisPlayerAttack = AttackResult(targetPlayer, xCoord, yCoord);
            thisPlayer.Attacks.Add(thisPlayerAttack);

            if (thisPlayerAttack.HitResult == HitResult.Destroyed)
            {
                var ship = targetPlayer.Ships.FirstOrDefault(sh => sh.ShipUnits.Any(su => su.xCoord == xCoord && su.yCoord == yCoord)).ShipUnits.Select(s => new { x = s.xCoord, y = s.yCoord });
                await hub.Clients.User(thisPlayer.Id).onShipDestroyed(ship);
            }
            else await hub.Clients.User(thisPlayer.Id).onShotLanded(thisPlayerAttack.HitResult, xCoord, yCoord);

            if (thisPlayerAttack.HitResult != HitResult.Missed && targetPlayer.Ships.SelectMany(s => s.ShipUnits).All(su => su.Destroyed))
                Gameover(currentBattle, targetPlayer.Id == currentBattle.Player1Id ? BattleOutcome.Player2Won : BattleOutcome.Player1Won);
            else switch (currentBattle.Mode)
                {
                    case BattleMode.vsCPU:
                        if (thisPlayerAttack.HitResult == HitResult.Missed)
                        {
                            Attack cpuAttack;
                            do
                            {
                                cpuAttack = ArtificialIntelligence.GenerateAttack(targetPlayer.Attacks.ToList());
                                cpuAttack = AttackResult(thisPlayer, cpuAttack.xCoord, cpuAttack.yCoord);
                                targetPlayer.Attacks.Add(cpuAttack);
                                await hub.Clients.User(thisPlayer.Id).onGettingShot(cpuAttack.xCoord, cpuAttack.yCoord);
                            }
                            while (cpuAttack.HitResult != HitResult.Missed);

                            await hub.Clients.User(thisPlayer.Id).openFire();
                        }
                        break;
                    case BattleMode.vsPlayer:
                        await hub.Clients.User(targetPlayer.Id).onGettingShot(xCoord, yCoord);
                        if (thisPlayerAttack.HitResult == HitResult.Missed)
                            await hub.Clients.User(targetPlayer.Id).openFire();
                        break;
                    default:
                        break;
                }
            dbContext.SaveChanges();
        }

        [HttpPost]
        public async Task FindBattleVersusCPU(List<Coordinate> coords)
        {
            Player thisPlayer = initializePlayer();

            if (thisPlayer == null)
                return;

            //если есть активный бой, но пользователь предпочел искать новый → заверишть предыдущий
            Battle battle = DetermineBattle();// dbContext.Battles.FirstOrDefault(b => b.Status == BattleStatus.InAction && (b.Player1Id == thisPlayer.Id || b.Player2Id == thisPlayer.Id));
            if (battle != null)
                Gameover(battle, thisPlayer.Id == battle.Player1Id ? BattleOutcome.Player1Disconnected : BattleOutcome.Player2Disconnected);


            if (thisPlayer.Ships.Any())
            {
                dbContext.Ships.RemoveRange(thisPlayer.Ships);
                thisPlayer.Ships.Clear();
            }

            thisPlayer.Ships = BuildShips(coords);

            Player bot = new Player()
            {
                Id=dbContext.Users.Single(u=>u.Email==Settings.Default.ServiceAccountEmail).Id,
                Nickname = "The bot",
                Ships = ArtificialIntelligence.DislocateShips()
            };

            dbContext.Players.Add(bot);
            dbContext.SaveChanges();

            await StartBattle(thisPlayer.Id, bot.Id, BattleMode.vsCPU);

            //на всякий случай в пул всё равно пользователь отправляется, вдруг он или второй чувак отменит оффер
        }

        private void LeaveActiveBattle(Player thisPlayer)
        {
            Battle battle = dbContext.Battles.FirstOrDefault(b => b.Status == BattleStatus.InAction && (b.Player1Id == thisPlayer.Id || b.Player2Id == thisPlayer.Id));
            if (battle != null)
                Gameover(battle, thisPlayer.Id == battle.Player1Id ? BattleOutcome.Player1Disconnected : BattleOutcome.Player2Disconnected);
        }

        [HttpPost]
        public void ChallengePlayer(string targetConnectionId, List<Coordinate> coordinates)
        {
            Player thisPlayer = initializePlayer();

            LeaveActiveBattle(thisPlayer);
            ClearPlayerBattleHistory(thisPlayer);
            thisPlayer.Ships = BuildShips(coordinates);

            Player opponent = dbContext
                .PlayerSessions.Where(x => x.Finished == null && x.ConnectionId == targetConnectionId)
                .OrderBy(x => x.Started)
                .ToList()
                .Last()
                .Player;

            //Player opponent = dbContext.Players.FirstOrDefault(p => p.PlayerSessions.Where(w => w.Finished == null).OrderBy(ob => ob.Started).Last().ConnectionId == targetConnectionId);


            hub.Clients.User(opponent.Id).onOfferBattle(thisPlayer.Nickname);
            hub.Clients.Users(dbContext.PlayerSessions.Where(ps=>ps.Finished==null).Distinct(new PlayerSessionDistiguisher()).Select(s=>s.PlayerId).ToList()).onUserStateChanged(opponent.Id, opponent.Nickname, opponent.CurrentSession.Status, PlayerStatus.Negotiating);
            hub.Clients.Users(dbContext.PlayerSessions.Where(ps=>ps.Finished==null).Distinct(new PlayerSessionDistiguisher()).Select(s=>s.PlayerId).ToList()).onUserStateChanged(thisPlayer.Id, opponent.Nickname, opponent.CurrentSession.Status, PlayerStatus.Negotiating);

            PendingBattle newBattle = new PendingBattle()
            {
                Player1 = thisPlayer,
                Player2 = opponent,
                Player1Status = PlayerOfferStatus.Accepted,
                Player2Status = PlayerOfferStatus.Waiting,
                PendingBattleToken = Guid.NewGuid()
            };

            dbContext.PendingBattles.Add(newBattle);
            dbContext.SaveChanges();

            var cook = new HttpCookie(pendingBattleCookie, newBattle.PendingBattleToken.ToString()) { Expires = DateTime.Now + new TimeSpan(0, 30, 0) };
            hub.Clients.User(thisPlayer.Id).onSetCookie(cook);
            hub.Clients.User(opponent.Id).onSetCookie(cook);
        }

        #region OBSOLETE

        [HttpPost, Obsolete]
        public async Task FindBattleVersusPlayer()
        {
            Player thisPlayer = initializePlayer();
            if (thisPlayer == null)
                return;

            //если есть активный бой, но пользователь предпочел искать новый → заверишть предыдущий
            //тут не через детермин, чтобы если игрок себе куки разъебал, бой всё равно закончится
            LeaveActiveBattle(thisPlayer);

            RestartSearch(thisPlayer);
        }

        [HttpPost, Obsolete]
        public void RestartSearch()
        {
            Player thisPlayer = initializePlayer();
            RestartSearch(thisPlayer);
        }
        [Obsolete]
        private void RestartSearch(Player thisPlayer)
        {
            PendingBattle previousPendingBattle = DeterminePendingBattle();
            //если ты перезапустил поиск в то время, когда другой пользователь ждет от тебя ответа - отрубить ожидание
            if (previousPendingBattle!=null)
            {
                string otherPlayerConnectionId = previousPendingBattle.Player1Id == thisPlayer.Id ? 
                                                                                                    previousPendingBattle.Player2.PlayerSessions.Last(l=>l.Finished==null).ConnectionId 
                                                                                                  : previousPendingBattle.Player1.PlayerSessions.Last(l => l.Finished == null).ConnectionId;

                if (previousPendingBattle.Player1Id == thisPlayer.Id)
                    previousPendingBattle.Player1Status = PlayerOfferStatus.Ignored;
                else previousPendingBattle.Player2Status = PlayerOfferStatus.Ignored;

                hub.Clients.Client(otherPlayerConnectionId).onOpponentIgnored();

                previousPendingBattle.Closed = DateTime.Now;
            }

            Random R = new Random((int)DateTime.Now.Ticks);
            PendingBattle newBattle=null;
            Player randomPlayer = null;
            var otherWaitingPlayers = dbContext.WaitingPlayers.Where(wp => wp.PlayerId != thisPlayer.Id).ToList();

            if (otherWaitingPlayers.Count > 0)
                randomPlayer = otherWaitingPlayers[R.Next(0, otherWaitingPlayers.Count)].Player; 

            if (randomPlayer != null)
            {
                newBattle = new PendingBattle()
                {
                    Player1Id = thisPlayer.Id,
                    Player2Id = randomPlayer.Id/*, МОЯ ПОПЫТКА НЕ УВЕНЧАЛАСЬ УСПЕХОМ
                    PendingBattleToken = Guid.NewGuid(),
                    Player1 = thisPlayer,
                    Player2 = randomPlayer,
                    Player1Status =PlayerStatus.Waiting,
                    Player2Status = PlayerStatus.Waiting*/
                };
                dbContext.PendingBattles.Add(newBattle);
                hub.Clients.User(thisPlayer.Id).onOfferBattle(randomPlayer.Nickname);
                hub.Clients.Client(randomPlayer.CurrentConnectionId).onOfferBattle(thisPlayer.Nickname);

            }
            if (!dbContext.WaitingPlayers.Any(wp=>wp.PlayerId==thisPlayer.Id))
                dbContext.WaitingPlayers.Add(new WaitingPlayer() { Player = thisPlayer });

            dbContext.SaveChanges();

            if (newBattle != null)
            {
                var cook = new HttpCookie(pendingBattleCookie, newBattle.PendingBattleToken.ToString()) { Expires = DateTime.Now + new TimeSpan(0, 30, 0)};
                hub.Clients.User(thisPlayer.Id).onSetCookie(cook);
                hub.Clients.Client(randomPlayer.CurrentConnectionId).onSetCookie(cook);
            }
        }
        [HttpPost, Obsolete("В условиях прямых запросов типа 'писку даш ебат' нет смысла проверять статус оппонента, он полюбому согласен")]
        public async Task BattleOfferReply2(bool agreed)
        {
            PendingBattle pBattle = DeterminePendingBattle();

            if (pBattle == null)
                return;

            Player thisPlayer = initializePlayer();
            Player opponent = pBattle.Player1Id == thisPlayer.Id ? pBattle.Player2 : pBattle.Player1;
            LeaveActiveBattle(opponent);
            PlayerOfferStatus res = agreed ? PlayerOfferStatus.Accepted : PlayerOfferStatus.Declined;

            if (thisPlayer.Id == pBattle.Player1Id)
                pBattle.Player1Status = res;
            else pBattle.Player2Status = res;

            switch (pBattle.Player1Id == thisPlayer.Id ? pBattle.Player2Status : pBattle.Player1Status)
            {
                case PlayerOfferStatus.Waiting:
                    if (!agreed)
                        hub.Clients.Client(opponent.CurrentConnectionId).onOpponentDeclined();
                    //else hub.Clients.Client(opponent.ConnectionId).onOpponentAccepted();
                    break;
                case PlayerOfferStatus.Accepted:
                    if (agreed)
                    {
                        hub.Clients.Client(opponent.CurrentConnectionId).onOpponentAccepted();
                        hub.Clients.User(thisPlayer.Id).onOpponentAccepted();
                    }
                    else
                    {
                        hub.Clients.Client(opponent.CurrentConnectionId).onOpponentDeclined();
                    }
                    break;
                case PlayerOfferStatus.Declined:
                    if (agreed)
                        hub.Clients.User(thisPlayer.Id).onOpponentDeclined();
                    break;
                case PlayerOfferStatus.Ignored:
                    hub.Clients.User(thisPlayer.Id).onOpponentIgnored();
                    break;
                default:
                    break;
            }
            dbContext.SaveChanges();
        }

        [HttpPost, Obsolete]
        public async Task ReadyForBattle(List<Coordinate> coords)
        {
            Player thisPlayer = initializePlayer();
            PendingBattle pBattle = DeterminePendingBattle();

            if (pBattle == null)
                return;
            ClearPlayerBattleHistory(thisPlayer);
            thisPlayer.Ships = BuildShips(coords);///???
            Battle newBattle = null;
            if (thisPlayer.Id == pBattle.Player1Id)
            {
                pBattle.Player1Status = PlayerOfferStatus.Ready;
                if (pBattle.Player2Status == PlayerOfferStatus.Ready)
                    newBattle = await StartBattle(pBattle.Player1Id, pBattle.Player2Id, BattleMode.vsPlayer);
            }
            else
            {
                pBattle.Player2Status = PlayerOfferStatus.Ready;
                if (pBattle.Player1Status == PlayerOfferStatus.Ready)
                    newBattle = await StartBattle(pBattle.Player1Id, pBattle.Player2Id, BattleMode.vsPlayer);
            }

            if (newBattle != null)
            {
                pBattle.Closed = DateTime.Now;
            }
            dbContext.SaveChanges();
        }

        #endregion


        [HttpPost]
        public async Task CancelInvite()
        {
            Player thisPlayer = initializePlayer();
            PendingBattle pBattle = DeterminePendingBattle();
            Player opponent = pBattle.Player1Id == thisPlayer.Id ? pBattle.Player2 : pBattle.Player1;
            if (pBattle.Player1Id == thisPlayer.Id)
            {
                pBattle.Player1Status = PlayerOfferStatus.Canceled;
                pBattle.Player2Status = PlayerOfferStatus.Ignored;
            }
            else
            {
                pBattle.Player2Status = PlayerOfferStatus.Canceled;
                pBattle.Player1Status = PlayerOfferStatus.Ignored;
            }
            pBattle.Closed = DateTime.Now;
            hub.Clients.User(opponent.Id).onInviteCanceled();
        }

        [HttpPost]
        public async Task BattleOfferReply(bool agreed, List<Coordinate> coords)
        {
            PendingBattle pBattle = DeterminePendingBattle();
            PlayerStatus newStatus;
            if (pBattle == null)
                return;

            Player thisPlayer = initializePlayer();
            Player opponent = pBattle.Player1Id == thisPlayer.Id ? pBattle.Player2 : pBattle.Player1;

            PlayerOfferStatus res = agreed ? PlayerOfferStatus.Accepted : PlayerOfferStatus.Declined;
            if (agreed && coords!=null&&coords.Count==20)
            {
                LeaveActiveBattle(thisPlayer);
                ClearPlayerBattleHistory(thisPlayer);
                thisPlayer.Ships = BuildShips(coords);

                hub.Clients.User(opponent.Id).onOpponentAccepted();
                hub.Clients.User(thisPlayer.Id).onOpponentAccepted();

                newStatus = PlayerStatus.InBattle;
                await StartBattle(opponent.Id, thisPlayer.Id, BattleMode.vsPlayer);
            }
            else
            {
                hub.Clients.User(opponent.Id).onOpponentDeclined();
                newStatus = PlayerStatus.Online;
            }

            pBattle.Closed = DateTime.Now;
            if (pBattle.Player1Id == opponent.Id)
                pBattle.Player1Status = res;
            else pBattle.Player2Status = res;

            thisPlayer.CurrentSession.Status = PlayerStatus.InBattle;
            opponent.CurrentSession.Status = PlayerStatus.InBattle;

            hub.Clients.AllExcept(opponent.CurrentConnectionId).onUserStateChanged(opponent.CurrentConnectionId, opponent.Nickname, PlayerStatus.Negotiating, PlayerStatus.InBattle);
            hub.Clients.AllExcept(thisPlayer.CurrentConnectionId).onUserStateChanged(thisPlayer.CurrentConnectionId, thisPlayer.Nickname, PlayerStatus.Negotiating, PlayerStatus.InBattle);

            dbContext.SaveChanges();
        }



        private PendingBattle DeterminePendingBattle()
        {
            if (HttpContext.Request.Cookies.AllKeys.Any(c => c == pendingBattleCookie))
            {
                Guid token;
                if (Guid.TryParse(HttpContext.Request.Cookies[pendingBattleCookie].Value, out token))
                {
                    var rezult = dbContext.PendingBattles.Where(pb => pb.PendingBattleToken == token).ToList().FirstOrDefault(pb=>pb.IsActive);
                    if (rezult == null)
                        HttpContext.Response.Cookies.Remove(pendingBattleCookie);
                    else return rezult;
                }
            }
            return null;
        }
        private Battle DetermineBattle()
        {
            if (HttpContext.Request.Cookies.AllKeys.Any(c => c == battleCookie))
            {
                Guid token;
                if (Guid.TryParse(HttpContext.Request.Cookies[battleCookie].Value, out token))
                {
                    var rezult = dbContext.Battles.Where(pb => pb.BattleToken == token).ToList().FirstOrDefault(b=>b.Status==BattleStatus.InAction && b.Finished==null);
                    if (rezult == null)
                        HttpContext.Response.Cookies.Remove(battleCookie);
                    else return rezult;
                }
            }
            return null;
        }

        private Player initializePlayer()
        {
            string currentUserId = User.Identity.GetUserId();
            if (currentUserId!=null)
                return dbContext.Players.FirstOrDefault(p => p.Id == currentUserId);
            return null;
        }


        [HttpPost]
        public void SendMessage(string message)
        {
            Player thisPlayer = initializePlayer();
            hub.Clients.All.onNewMessage(thisPlayer.Nickname, message);
            //не через детермин потому что типа б-безопасность, чтоб нельзя было куки подкидывать и срать в чужих играх
            Battle activeBattle = dbContext.Battles.FirstOrDefault(b => b.Player1Id == thisPlayer.Id || b.Player2Id == thisPlayer.Id);
            if (activeBattle == null)
                return;

            hub.Clients.Group(activeBattle.BattleToken.ToString()).onNewMessage(thisPlayer.Nickname, message);
        }

        internal List<Ship> BuildShips(List<Coordinate> coords)
        {
            List<List<ShipUnit>> ships = new List<List<ShipUnit>>();

            List<ShipUnit> shipContainer;
            for (int i = 0; i < coords.Count; i++)
            {
                shipContainer = ships.FirstOrDefault(su => su.Any(c => (Math.Abs(c.xCoord - coords[i].xCoord) == 1 && c.yCoord - coords[i].yCoord == 0)
                                                                    || (Math.Abs(c.yCoord - coords[i].yCoord) == 1 && c.xCoord - coords[i].xCoord == 0)));

                if (shipContainer == null)
                    ships.Add(new List<ShipUnit>() { new ShipUnit() { xCoord = coords[i].xCoord, yCoord = coords[i].yCoord } });
                else shipContainer.Add(new ShipUnit() { xCoord = coords[i].xCoord, yCoord = coords[i].yCoord });

            }

            return ships.Select(s => new Ship() { ShipUnits = s }).ToList();
        }

        
        private void ClearPlayerBattleHistory (Player thisPlayer)
        {
            dbContext.Ships.RemoveRange(thisPlayer.Ships);
            thisPlayer.Ships.Clear();
            dbContext.Attacks.RemoveRange(thisPlayer.Attacks);
            thisPlayer.Attacks.Clear();
        }

        internal async Task<Battle> StartBattle(string player1Id, string player2Id, BattleMode bMode)
        {
            dbContext.WaitingPlayers.RemoveRange(dbContext.WaitingPlayers.Where(wp => wp.PlayerId == player1Id || wp.PlayerId == player2Id));
            dbContext.SaveChanges();

            Battle newBattle = new Battle()
            {
                Started = DateTime.Now,
                Player1Id = player1Id,
                Player2Id = player2Id,
                Status = BattleStatus.InAction,
                BattleToken = Guid.NewGuid(),
                Mode = bMode,
            };
            dbContext.Battles.Add(newBattle);
            dbContext.SaveChanges();

            if (bMode == BattleMode.vsPlayer)
            {
                Player player1 = dbContext.Players.Find(player1Id);
                Player player2 = dbContext.Players.Find(player2Id);

                Random R = new Random((int)DateTime.Now.Ticks);
                bool goFirst = R.Next(0, 100) <= 50;

                hub.Clients.User(player1.Id).onSetCookie(new HttpCookie(battleCookie, newBattle.BattleToken.ToString()) { Expires = DateTime.Now + new TimeSpan(0, 60, 0) });
                hub.Clients.User(player2.Id).onSetCookie(new HttpCookie(battleCookie, newBattle.BattleToken.ToString()) { Expires = DateTime.Now + new TimeSpan(0, 60, 0) });

                await hub.Clients.User(player1.Id).onBattleStarted(goFirst);
                await hub.Clients.User(player2.Id).onBattleStarted(!goFirst);

                PlayerSession p1Session = player1.PlayerSessions.Last(l => l.Finished == null);
                hub.Clients.All.onUserStateChanged(player1.CurrentConnectionId, player1.Nickname, p1Session.Status, PlayerStatus.InBattle);
                p1Session.Status = PlayerStatus.InBattle;

                PlayerSession p2Session = player2.PlayerSessions.Last(l => l.Finished == null);
                hub.Clients.All.onUserStateChanged(player1.CurrentConnectionId, player1.Nickname, p2Session.Status, PlayerStatus.InBattle);
                p2Session.Status = PlayerStatus.InBattle;
            }
            else
            {
                Player thisPlayer = initializePlayer();
                hub.Clients.User(thisPlayer.Id).onSetCookie(new HttpCookie(battleCookie, newBattle.BattleToken.ToString()) { Expires = DateTime.Now + new TimeSpan(0, 60, 0) });
            }
            return newBattle;
        }

        internal void Gameover(Battle battle, BattleOutcome otc)
        {
            battle.Finished = DateTime.Now;
            battle.BattleResult = otc;
            battle.Status = BattleStatus.Finished;

            Player thisPlayer = initializePlayer();

            bool won = thisPlayer.Id == battle.Player1Id ? (otc == BattleOutcome.Player1Won || otc == BattleOutcome.Player2Disconnected) : (otc == BattleOutcome.Player2Won || otc == BattleOutcome.Player1Disconnected);
            switch (battle.Mode)
            {
                case BattleMode.vsCPU:
                    hub.Clients.User(thisPlayer.Id).onGameover("комплюктер", otc, won);
                    ClearPlayerBattleHistory(thisPlayer);
                    break;
                case BattleMode.vsPlayer:
                    Player opponent = battle.Player1Id == thisPlayer.Id ? battle.Player2 : battle.Player1;
                    ClearPlayerBattleHistory(thisPlayer);
                    ClearPlayerBattleHistory(opponent);
                    
                    //не самая лучшая идея вызывать onGameover через группу. игра может закончиться, из за того, что один из игроков не захотел ее продолжать,
                    //тогда после дисконнекта его в группе уже не будет. не добавлять же его заного, чтобы потом тут же удалить оттуда
                    hub.Clients.User(thisPlayer.Id).onGameover(opponent.Nickname, otc, won);
                    hub.Clients.User(opponent.Id).onGameover(thisPlayer.Nickname, otc, !won);

                    //hub.Groups.Remove(thisPlayer.Id, battle.BattleToken.ToString());
                    //hub.Groups.Remove(opponent.Id, battle.BattleToken.ToString());
                    break;
                default:
                    break;
            }
        }
        internal Attack AttackResult(Player targetPlayer, short xCoord, short yCoord)
        {
            ShipUnit wreckedUnit = targetPlayer.Ships.SelectMany(s => s.ShipUnits)
                                                            .FirstOrDefault(su => su.xCoord == xCoord && su.yCoord == yCoord && !su.Destroyed);
            Attack a = new Attack() { xCoord = xCoord, yCoord = yCoord };
            if (wreckedUnit != null)
            {
                wreckedUnit.Destroyed = true;
                if (wreckedUnit.Ship.ShipUnits.All(su => su.Destroyed))
                    a.HitResult = HitResult.Destroyed;
                else a.HitResult = HitResult.Hit;
            }
            else a.HitResult = HitResult.Missed;
            return a;
        }
    }
}
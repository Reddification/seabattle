﻿angular.module('seaBattleApp')
    .directive('draggable', function () {
        return {
            restrict: "A",
            link: function (scope, element, attrs) {
                scope.width = element[0].offsetWidth;
                scope.height = element[0].offsetHeight;
                scope.battlefield = $("#" + attrs["battlefield"]);

                element.bind('dblclick', function () {
                    rotateSheap();
                });

                element.css({ top: scope.ship.startPosition.y * scope.height + 'px' });
                element.css({ left: scope.ship.startPosition.x * scope.width + 'px' });

                var borderWidth = (element[0].offsetWidth - element[0].clientWidth) / 2;

                if (scope.ship.isVertical) {
                    element.width(element[0].offsetWidth - borderWidth);
                    element.height(scope.ship.weight * element[0].offsetHeight - borderWidth);
                }
                else {
                    element.width(scope.ship.weight * element[0].offsetWidth - borderWidth);
                    element.height(element[0].offsetHeight - borderWidth);
                }

                element.draggable({
                    containment: ".dragCell",
                    appendTo: ".dragCell",
                    snapMode: "outer",
                    drag: function (event, ui) {
                        clearCells();
                        scope.ship.startPosition.x = Math.round(ui.position.left / scope.width);
                        scope.ship.startPosition.y = Math.round(ui.position.top / scope.height);
                        if (!canDropSheap(scope.ship)) {
                            element.addClass("error");
                            scope.canDrop = false;
                        } else {
                            element.removeClass("error");
                            scope.canDrop = true;
                            drawCells();
                        }
                    },
                    start: function (event, ui) {
                        scope.startPosition = angular.copy(scope.ship.startPosition);
                    },
                    stop: function (event, ui) {
                        clearCells();
                        if (!scope.canDrop) {
                            scope.ship.startPosition = scope.startPosition;
                        }
                        element.css({ top: scope.ship.startPosition.y * scope.height + 'px' });
                        element.css({ left: scope.ship.startPosition.x * scope.width + 'px' });
                        element.removeClass("error");
                        scope.canDrop = true;
                        console.log("stop");
                    },
                    delay: 100
                });

                function canDropSheap(movedSheap) {
                    var asd = scope.ships.find(x => x.Id != movedSheap.Id &&
                        (
                            (x.isVertical === movedSheap.isVertical &&
                                (
                                    (x.isVertical === false && checkSameHorizontalOrientation(movedSheap, x)) ||
                                    (x.isVertical === true && checkSameVerticalOrientation(movedSheap, x))
                                )
                            ) ||
                            (
                                (x.isVertical != movedSheap.isVertical) &&
                                (
                                    (movedSheap.isVertical && checkDiffentOrientation(x, movedSheap)) ||
                                    (!movedSheap.isVertical && checkDiffentOrientation(movedSheap, x))
                                )
                            )
                        )
                    );
                    console.log(asd);
                    return (scope.ships.find(x => x.Id != movedSheap.Id &&
                        (
                            (x.isVertical === movedSheap.isVertical &&
                                (
                                    (x.isVertical === false && checkSameHorizontalOrientation(movedSheap, x)) ||
                                    (x.isVertical === true && checkSameVerticalOrientation(movedSheap, x))
                                )
                            ) ||
                            (
                                (x.isVertical != movedSheap.isVertical) &&
                                (
                                    (movedSheap.isVertical && checkDiffentOrientation(x, movedSheap)) ||
                                    (!movedSheap.isVertical && checkDiffentOrientation(movedSheap, x))
                                )
                            )
                        )
                    ) != undefined) ? false : true;
                }
                //Проверка двух кораблей с различными ориентациями
                function checkDiffentOrientation(horizontalSheap, verticalSheap) {
                    return (
                        (Math.abs(horizontalSheap.startPosition.x - verticalSheap.startPosition.x) < 2 || Math.abs(horizontalSheap.startPosition.x + horizontalSheap.weight - 1 - verticalSheap.startPosition.x) < 2) &&
                        (Math.abs(verticalSheap.startPosition.y - horizontalSheap.startPosition.y) < 2 || Math.abs(verticalSheap.startPosition.y + verticalSheap.weight - 1 - horizontalSheap.startPosition.y) < 2)
                    )
                }
                //Проверка двух вертикальных кораблей
                function checkSameVerticalOrientation(movedSheap, otherShip) {
                    return (
                        (Math.abs(movedSheap.startPosition.x - otherShip.startPosition.x) < 2) &&
                        (
                            (movedSheap.startPosition.y >= otherShip.startPosition.y && movedSheap.startPosition.y <= (otherShip.startPosition.y + otherShip.weight)) ||
                            ((movedSheap.startPosition.y + movedSheap.weight) >= (otherShip.startPosition.y) && (movedSheap.startPosition.y + movedSheap.weight - 1) <= (otherShip.startPosition.y + otherShip.weight))
                        )
                    )
                }
                //Проверка двух горизонтальных кораблей
                function checkSameHorizontalOrientation(movedSheap, otherShip) {
                    return (
                        (Math.abs(movedSheap.startPosition.y - otherShip.startPosition.y) < 2) &&
                        (
                            (movedSheap.startPosition.x >= otherShip.startPosition.x && movedSheap.startPosition.x <= (otherShip.startPosition.x + otherShip.weight)) ||
                            ((movedSheap.startPosition.x + movedSheap.weight) >= (otherShip.startPosition.x) && (movedSheap.startPosition.x + movedSheap.weight) <= (otherShip.startPosition.x + otherShip.weight))
                        )
                    )
                }

                function rotateSheap() {
                    console.log("rotate");
                    var currentSheap = scope.ship;
                    currentSheap.isVertical = !currentSheap.isVertical;
                    if (currentSheap.isVertical) {
                        if (currentSheap.startPosition.y + currentSheap.weight >= 10)
                            currentSheap.startPosition.y = 10 - currentSheap.weight;
                    } else
                        if (currentSheap.startPosition.x + currentSheap.weight >= 10)
                            currentSheap.startPosition.x = 10 - currentSheap.weight;

                    if (canDropSheap(currentSheap)) {
                        scope.ship = (currentSheap);
                        drawSheap();
                        console.log('Можно');
                    }
                    else
                        console.log('Нельзя');
                }

                function drawCells() {
                    for (var i = 0; i < scope.ship.weight; i++)
                        if (scope.ship.isVertical)
                            scope.battlefield.find("tr").eq(scope.ship.startPosition.y + 1 + i).find("td").eq(scope.ship.startPosition.x).addClass("successDrop");
                        else
                            scope.battlefield.find("tr").eq(scope.ship.startPosition.y + 1).find("td").eq(scope.ship.startPosition.x + i).addClass("successDrop");
                }
                function clearCells() {
                    for (var i = 0; i < scope.ship.weight; i++)
                        if (scope.ship.isVertical)
                            scope.battlefield.find("tr").eq(scope.ship.startPosition.y + 1 + i).find("td").eq(scope.ship.startPosition.x).removeClass("successDrop");
                        else
                            scope.battlefield.find("tr").eq(scope.ship.startPosition.y + 1).find("td").eq(scope.ship.startPosition.x + i).removeClass("successDrop");
                }

                function drawSheap() {
                    var top = scope.ship.startPosition.y * scope.height;
                    var left = scope.ship.startPosition.x * scope.width;
                    element.css({ top: scope.ship.startPosition.y * scope.height + 'px' });
                    element.css({ left: scope.ship.startPosition.x * scope.width + 'px' });
                    if (scope.ship.isVertical) {
                        element.width(scope.width - borderWidth);
                        element.height(scope.ship.weight * scope.height - borderWidth);
                    }
                    else {
                        element.width(scope.ship.weight * scope.width - borderWidth);
                        element.height(scope.height - borderWidth);
                    }
                }
            },
            scope: {
                ships: '=',
                ship: '=draggable'
            }
        }
    });
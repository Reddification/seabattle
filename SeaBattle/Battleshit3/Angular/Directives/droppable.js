﻿angular.module('seaBattleApp')
    .directive('droppable', function () {
        return {
            restrict: "A",
            link: function (scope, element, attrs) {
                element.droppable({});
            }
        }
    })
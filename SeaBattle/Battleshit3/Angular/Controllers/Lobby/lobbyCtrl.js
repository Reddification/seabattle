﻿angular.module("seaBattleApp")
    .controller("lobbyCtrl", function ($scope, config, dataService, $ngConfirm, $cookies) {

        $scope.$on('onGetUserList', function (event, data) {
            $scope.players = data;
        })

        $scope.selectOpponent = function (ConnectionId) {
            $scope.showDialog('Запрос', 'Вы уверены в своем выборе?', $scope.challengePlayer, undefined, { success: [ConnectionId] });
        }
        $scope.challengePlayer = function (ConnectionId) {           
            var config = {
                url: $scope.getUrl("Battle", "ChallengePlayer"),
                params: {
                    targetConnectionId: ConnectionId,
                    coordinates: $scope.getShipsArray()
                },
                method: "POST"
            };
            var responsePromise = dataService.sendRequest(config);
            responsePromise.then(function (response) {
                console.log(response.data.Data);
            });
        }        
    })
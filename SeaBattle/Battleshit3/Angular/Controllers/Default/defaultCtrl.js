﻿angular.module("seaBattleApp")
    .controller("defaultCtrl", function ($scope, config, dataService, $ngConfirm, $cookies, notifyService, audioNotifyService) {
        $scope.columnsHeaders = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
        $scope.versusCPU = true;
        $scope.rowsHeaders = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        $scope.opponentFields = $("#rivalBattlefield");
        $scope.myFields = $("#battlefield");

        $scope.ships = [];
        $scope.opponentBattleField = [];

        $scope.startedGame = false;
        $scope.openFire = false;
       
        $scope.players = [];


        $scope.initLoad = false;
        $scope.defaultInit = function () {
            console.log("default init");
            $scope.connectSignalR();
            $scope.initializeState();
        }
        $scope.startGame = function () {
            $scope.startedGame = true;
            if ($scope.versusCPU)
                $scope.findBattleVersusCPU();
            else {
                if ($scope.selectedPlayer != undefined)
                    $scope.challengePlayer();
                else
                    alert('Gbplfr');
            }            
        }

        //////////////////////////////////
        ////////////SignalR///////////////
        //////////////////////////////////
        $scope.battleshipHub = $.connection.battleship;
        $scope.connectSignalR = function () {
            $.connection.hub.start()
                .done(function () {
                    console.log('SignalR connected. ConnectionID: ' + $.connection.hub.id);
                    //$scope.register();
                })
                .fail(function () {
                    console.error('Error SignalR connection');
                });
        }

        $scope.battleshipHub.client.onOpponnentDisconnected = function () {
            console.log("onOpponnentDisconnected -signalR")
        }
        $scope.battleshipHub.client.onConnected = function (haveActiveBattles, opponentNickname, startDate) {
            console.info("onConnected -signalR");
            if (haveActiveBattles == true) {
                $scope.showDialog('Запрос', 'Имеется незаконченный бой с ' + opponentNickname + ', начатый в ' + startDate + '. Не желаете продолжить?', $scope.battleOfferReply, $scope.battleOfferReply, { success: [true], failure: [false] });
                $scope.opponentNickname = opponentNickname;
            }            
        }
        $scope.battleshipHub.client.onGetUserList = function (users) {
            //$scope.$broadcast('onGetUserList', users);
            $scope.players = users;
            console.log($scope.players);
            $scope.$digest();
            console.log('onGetUserList  - signalr');
        }
        $scope.battleshipHub.client.onUserStateChanged = function (ConnectionId, nickname, status, playerStatus) {
            console.log('onUserStateChanged - signalR');
            var index = -1;
            if ($scope.players != undefined && $scope.players.length > 0)
                index = $scope.players.indexOf(x => x.ConnectionId == ConnectionId);
            if (index == -1) {
                $scope.players.push({ ConnectionId: ConnectionId, Nickname: nickname, Status: playerStatus })
            } else {
                $scope.players[index].ConnectionId = ConnectionId;
                $scope.players[index].Nickname = nickname;
                $scope.players[index].Status = playerStatus;
                if ($scope.selectedPlayer.ConnectionId == ConnectionId)
                    $scope.selectedPlayer = undefined;
            }
        }
        $scope.battleshipHub.client.onOfferBattle = function (opponentNickname) {
            console.info("onOfferBattle -signalR")
            $scope.showDialog('Игрок ' + opponentNickname + ' предлагает вам сыграть вместе. Вы согласны?', '', $scope.battleOfferReply, $scope.battleOfferReply, { success: [true], failure: [false] });
            $scope.opponentNickname = opponentNickname;
        }
        $scope.battleshipHub.client.onGameover = function (opponentNickname, BattleOutcome, won) {            
            console.info("onGameover -signalR");
            if (won) {
                var config = {
                    title: "Победа!!!",
                    text:"Принимите наши поздравления",
                    type: "success"
                };  
                audioNotifyService.play('winner');
            } else {
                var config = {
                    title: "Поражение...",
                    text: "Увы, в следующий раз вам точно повезет",
                    type: "error"
                };
                audioNotifyService.play('looser');
            }
            notifyService.show(config);
            $scope.endGame = true;
            $scope.$digest();
        }
        $scope.battleshipHub.client.onBattleStarted = function (goFirst) {
            console.info("onBattleStarted -signalR");
            $scope.initializeOpponentBattleField();
            $('#rivalBattlefield').css('opacity', 1);
            $scope.startedGame = true;
            if (goFirst) {
                $scope.message = "Ваш ход";
                $scope.openFire = true;
            } else {
                $scope.message = "Ожидание хода соперника";
                $scope.openFire = false;
            }
            $scope.$digest();
        }
        $scope.battleshipHub.client.onShipDestroyed = function (ship) {
            for (var i = 0; i < ship.length; i += 1) {
                var cell = $scope.opponentFields.find("tr").eq(ship[i].y + 1).find("td").eq(ship[i].x)
                cell.addClass("ship-destroy");
                $scope.drawX(cell.children()[0].children[0]);
                $scope.openFire = true;
            }
        }
        //Hit = 1,
        //Missed = 2,
        //Destroyed = 3
        $scope.battleshipHub.client.onShotLanded = function (hitResult, xCoord, yCoord) {
            if (hitResult == 1) {
                $scope.drawX($scope.opponentFields.find("tr").eq(yCoord + 1).find("td").eq(xCoord).children()[0].children[0])
                $scope.openFire = true;
                audioNotifyService.play('hit');
            } else if (hitResult == 2) {
                $scope.drawO($scope.opponentFields.find("tr").eq(yCoord + 1).find("td").eq(xCoord).children()[0].children[0])
                $scope.openFire = false;
                audioNotifyService.play('missed');
                $scope.message = "Ожидание хода соперника";
            } else {
                console.log("destroy");
            }
            console.info("onShotLanded - signalR");
        }
        $scope.battleshipHub.client.onGettingShot = function (xCoord, yCoord) {
            var ship = $scope.ships.find(x =>
                (xCoord >= x.startPosition.x && xCoord <= x.startPosition.x + x.weight -1 && x.startPosition.y == yCoord && x.isVertical == false) ||
                (xCoord == x.startPosition.x && yCoord >= x.startPosition.y && yCoord <= x.startPosition.y + x.weight -1 && x.isVertical == true));
            //Промах
            if (ship == undefined) {
                $scope.drawO($scope.myFields.find("tr").eq(yCoord + 1).find("td").eq(xCoord).children()[0].children[0])
                audioNotifyService.play('missed');
                $scope.openFire = true;
            } else {
                $scope.drawX($scope.myFields.find("tr").eq(yCoord + 1).find("td").eq(xCoord).children()[0].children[0]);
                audioNotifyService.play('hit');
                $scope.openFire = false;
            }
            console.info("onGettingShot - signalR");
        }
        $scope.battleshipHub.client.onRematch = function (PendingBattleId) {
            console.log("onRematch -signalR")
        }
        $scope.battleshipHub.client.onNewMessage = function (nickname, message) {
            console.log("onNewMessage -signalR")
        }        
        $scope.battleshipHub.client.openFire = function () {
            $scope.message = "Ваш ход";
            $scope.openFire = true;
            $scope.$digest();
            console.log("openFire -signalR")
        }
        $scope.battleshipHub.client.onSetCookie = function (cookie) {
            console.log('onSetCookie -  signaalR:');
            $cookies.put(cookie.Name, cookie.Value, {path:'/'});
            console.log(cookie);
        }
        $scope.battleshipHub.client.onOpponentAccepted = function () {
            console.log('signalR - onOpponentAccepted');
            //$scope.readyForBattle();
        }


        //////////////////////////////////
        ////////////Support///////////////
        //////////////////////////////////
        $scope.getUrl = function (controller, method) {
            return config.apiURL + controller + "/" + method;
        }
        $scope.showDialog = function (title, content, actionSuccess, actionFailure, params) {

            $ngConfirm({
                title: title,
                content: content,
                scope: $scope,
                theme: 'dark',
                buttons: {
                    No: {
                        text: 'Нет',
                        action: function () {
                            if (actionFailure!= undefined)
                                actionFailure.apply(this, params.failure);
                        }
                    },
                    Yes: {
                        text: "Да",
                        action: function () {
                            if (actionSuccess != undefined)
                                actionSuccess.apply(this, params.success);
                        }
                    }
                }
            });
        }
        $scope.getShipsArray = function () {
            var arr = [];
            $scope.ships.map(function (item) {
                {                    
                    for (var i = 0; i < item.weight; i++) {
                        arr.push(item.isVertical ? {
                            xCoord: item.startPosition.x,
                            yCoord: item.startPosition.y + i
                        } :
                            {
                                xCoord: item.startPosition.x + i,
                                yCoord: item.startPosition.y
                            })
                    }
                }
            })
            return arr;
        }
        $scope.initializeOpponentBattleField = function () {
            $scope.opponentBattleField = [];
            for (var i = 0; i < $scope.rowsHeaders.length; i += 1) {
                $scope.opponentBattleField.push([]);
                for (var j = 0; j < $scope.columnsHeaders.length; j += 1) {
                    $scope.opponentBattleField[i].push({ isAttacked: false });
                }
            }
        }
        $scope.initializeState = function () {
            $scope.ships = [
                {
                    "Id": 1,
                    "startPosition": {
                        x: 1,
                        y: 0
                    },
                    "weight": 1,
                    "isVertical": true
                },
                {
                    "Id": 2,
                    "startPosition": {
                        x: 3,
                        y: 4
                    },
                    "weight": 1,
                    "isVertical": true
                },
                {
                    "Id": 3,
                    "startPosition": {
                        x: 5,
                        y: 6
                    },
                    "weight": 1,
                    "isVertical": true
                },
                {
                    "Id": 4,
                    "startPosition": {
                        x: 4,
                        y: 9
                    },
                    "weight": 1,
                    "isVertical": true
                },
                {
                    "Id": 5,
                    "startPosition": {
                        x: 0,
                        y: 5
                    },
                    "weight": 2,
                    "isVertical": true
                },
                {
                    "Id": 6,
                    "startPosition": {
                        x: 6,
                        y: 4
                    },
                    "weight": 2,
                    "isVertical": false
                },
                {
                    "Id": 7,
                    "startPosition": {
                        x: 0,
                        y: 8
                    },
                    "weight": 2,
                    "isVertical": false
                },
                {
                    "Id": 8,
                    "startPosition": {
                        x: 9,
                        y: 3
                    },
                    "weight": 3,
                    "isVertical": true
                },
                {
                    "Id": 9,
                    "startPosition": {
                        x: 0,
                        y: 2
                    },
                    "weight": 3,
                    "isVertical": false
                },
                {
                    "Id": 10,
                    "startPosition": {
                        x: 5,
                        y: 1
                    },
                    "weight": 4,
                    "isVertical": false
                },
            ]            
            for (var i = 0; i < $scope.opponentBattleField.length; i += 1) {
                for (var j = 0; j < $scope.opponentBattleField[i].length; j += 1) {
                    $scope.clearCell($scope.myFields.find("tr").eq(i + 1).find("td").eq(j).children()[0].children[0]);
                    $scope.myFields.find("tr").eq(i + 1).find("td").eq(j).removeClass("ship-destroy");
                    $scope.clearCell($scope.opponentFields.find("tr").eq(i + 1).find("td").eq(j).children()[0].children[0]);
                    $scope.opponentFields.find("tr").eq(i + 1).find("td").eq(j).removeClass("ship-destroy");
                }
            }
            $scope.initializeOpponentBattleField();
            $scope.openFire = false;
            $scope.startedGame = false;
            $scope.message = "Расставьте корабли и выберите соперника";
            $scope.initLoad = true;
            $scope.endGame = false;            
        }

        //////////////////////////////////
        //////////////API/////////////////
        //////////////////////////////////
        $scope.register = function () {
            var config = {
                url: $scope.getUrl("Battle", "Register"),
                params: {
                    HubConnectionId: $scope.battleshipHub.connection.id
                },
                method: "POST"
            };
            var responsePromise = dataService.sendRequest(config);
            responsePromise.then(function (response) {
                console.log(response.data.Data);
            });
        }
        $scope.battleOfferReply = function (isAgree) {
            var config = {
                url: $scope.getUrl("Battle", "BattleOfferReply"),
                data: {
                    agreed: isAgree,
                    coords: $scope.getShipsArray()
                },
                method: "POST"
            };
            var responsePromise = dataService.sendRequest(config);
            responsePromise.then(function (response) {
                console.log(response.data);
            });
        }
        $scope.findBattleVersusCPU = function () {
            var config = {
                url: $scope.getUrl("Battle", "FindBattleVersusCPU"),
                data: {
                    coords: $scope.getShipsArray()
                },
                method:'POST'
            };
            var responsePromise = dataService.sendRequest(config);
            responsePromise.then(function (response) {
                console.log(response.data);
            });
            $scope.opponentNickname = "PC";
        }
        $scope.findBattleVersusPlayer = function () {
            var config = {
                url: $scope.getUrl("Battle", "FindBattleVersusPlayer"),
                data: {
                    nickname: $scope.username
                },
                method: 'POST'
            };
            var responsePromise = dataService.sendRequest(config);
            responsePromise.then(function (response) {
                console.log(response.data);
            });
        }
        $scope.readyForBattle = function () {
            var config = {
                url: $scope.getUrl("Battle", "ReadyForBattle"),
                data: {
                    coords: $scope.getShipsArray()
                },
                method: 'POST'
            };
            var responsePromise = dataService.sendRequest(config);
            responsePromise.then(function (response) {
                console.log(response.data);
            });
        }




        $scope.selectOpponent = function (player) {
            $scope.selectedPlayer = player;
            $scope.opponentNickname = player.Nickname;
        }
        $scope.challengePlayer = function (ConnectionId) {
            var config = {
                url: $scope.getUrl("Battle", "ChallengePlayer"),
                data: {
                    targetConnectionId: $scope.selectedPlayer.ConnectionId,
                    coordinates: $scope.getShipsArray()
                },
                method: "POST"
            };
            var responsePromise = dataService.sendRequest(config);
            responsePromise.then(function (response) {
                console.log(response.data.Data);
            });
        }    
        $scope.attack = function (x, y) {
            if ($scope.openFire) {
                $scope.openFire = false;
                console.log("attack");
                var config = {
                    url: $scope.getUrl("Battle", "Attack"),
                    data: {
                        xCoord: x,
                        yCoord: y
                    },
                    method: "POST"
                };
                var responsePromise = dataService.sendRequest(config);
                responsePromise.then(function (response) {
                    console.log(response.data);
                });
                $scope.opponentBattleField[x][y].isAttacked = true;
            }
        }

        $scope.drawX = function (canvas) {
            var context = canvas.getContext('2d');
            var width = canvas.width;
            var height = canvas.height;

            context.lineWidth = 5;
            context.beginPath();
            context.moveTo(0, 0);
            context.lineTo(width, height);
            context.stroke();


            context.beginPath();
            context.moveTo(0, height);
            context.lineTo(width, 0);
            context.stroke();
        }
        $scope.drawO = function (canvas) {
            var context = canvas.getContext('2d');
            var width = canvas.width;
            var height = canvas.height;
            var radius = 5;
            context.beginPath();
            context.arc(width / 2, height / 2, radius, 0, Math.PI * 2, false);
            context.closePath();
            context.fill(); 
        }
        $scope.clearCell = function (canvas) {
            var context = canvas.getContext('2d');
            context.clearRect(0, 0, canvas.width, canvas.height);
        }
    });
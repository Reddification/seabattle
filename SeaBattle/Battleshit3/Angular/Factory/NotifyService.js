﻿angular.module("seaBattleApp").factory('notifyService', function () {
    return {
        show: function (config) {
            PNotify.prototype.options.delay = config.delay == undefined ? 5000 : config.delay;
            PNotify.prototype.options.styling = config.styling == undefined ? "bootstrap3" : config.styling;
            new PNotify(config);
        }
    }
})
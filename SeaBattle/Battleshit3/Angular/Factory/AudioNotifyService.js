﻿angular.module("seaBattleApp").factory('audioNotifyService', function () {
    return {
        play: function (type) {
            if (type == 'gun') {
                var audio = new Audio('Audio/gun.mp3');
                audio.play();
            }
            if (type == 'hit') {
                var audio = new Audio('Audio/hit.mp3');
                audio.play();
            }
            if (type == 'missed') {
                var audio = new Audio('Audio/missed.mp3');
                audio.play();
            }
            if (type == 'winner') {
                var audio = new Audio('Audio/pobeda.mp3');
                audio.play();
            }
            if (type == 'looser') {
                var audio = new Audio('Audio/proigr.mp3');
                audio.play();
            }
        }
    }
})
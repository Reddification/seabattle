﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using System.Web.Http;
using Microsoft.AspNet.SignalR;

namespace Battleshit3
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //GlobalHost.Configuration.ConnectionTimeout = new TimeSpan(0, 5, 0);
            //GlobalHost.Configuration.DisconnectTimeout = new TimeSpan(0, 2, 0);
            //GlobalHost.Configuration.KeepAlive
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}

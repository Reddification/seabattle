﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Battleshit3.Models;
using System.Net;
using Battleshit3.AI;
using static Battleshit3.AI.ArtificialIntelligence;
using Microsoft.AspNet.Identity;

namespace Battleshit3.Hubs
{
    /** В хабе я дергаю события, обработчики которых предполагаются у тебя
         1. onOpponnentDisconnected - соперник отключился: закрыл вкладку, браузер, еще хуй знает что. 
         2. onConnected(haveActiveBattles, opponentNickname, startDate) - когда пользователь подключается, он может повторно зайти в бой, если хочет
         3. onGameover(opponentNickname, BattleOutcome) - при завершении игры у живых игроков дергается это события
         4. onOfferBattle(PendingBattleId, opponentNickname). на него я жду ответ в BattleOfferReply(pendingBattleId, agreed), где agreed - согласен на бой или нет
         5. onBattleStarted(goFirst) - игра началась, если goFirst=true, разблокируй клетки
         6. onRematch(PendingBattleId) - по сути, то же, что и onOfferBattle, на него я также жду ответ в BattleOfferReply(pendingBattleId, agreed), 
         7. onNewMessage(nickname, message) - чат. 
         8. onGettingShot(xCoord, yCoord) - оппонент стрельнул
         9. onShotLanded(xCoord, yCoord, hitResult) - коллбэк в ответ на Attack(xCoord,yCoord) с результатом атаки
         10. openFire() - приказ игроку открыть огонь. считай - разблокировать клетки вражеского корабля
         11. onError(errorCode, message)
    
        Обработчики на стороне сервера, вызывай их у себя
        1. BattleOfferReply(pendingBattleId, agreed) - ответ от игрока готов он бить ебало или нет
        2. Connect() - сразу в коллбэке $.connection.hub.start().done(function(){ } );
        3. FindBattle(coords, withAnotherPlayer, nickname) - по нажатию кнопки "погнали" вызываешь вот это, сюда координаты, с cpu/игроком, и никнейм на этот бой
        3.5 RestartSearch() можешь, например, каждые N секунд вызывать RestartSearch(), потому что если два игрока откажутся бить друг другу ебало,
                            то потом они смогут с кем то сыграть только когда подключится кто то третий, 
                            и типа они за время ожидания могут передумать и согласиться сыграть друг с другом
        4. Attack(xCoord, yCoord) - ну тут очевидно, куда уебер игрок
        5. DontWait() - на дисконнект игрока его оппоненту предлогается подождать реконнекта или забить и выйти. если он выходит, он всё равно выигрывает.
        6. SendMessage(message) - чат
        может, какие нибудь забыл, пробегись по коду
         */


    [HubName("battleship")]
    public class BattleshipHub:Hub
    {
        public override Task OnDisconnected(bool stopCalled)
        {
            //return base.OnDisconnected(stopCalled);
            using (BattleshipContext dbContext = new BattleshipContext())
            {
                string uID = Context.User.Identity.GetUserId();
                Player thisPlayer = dbContext.Players.FirstOrDefault(u=>u.Id==uID);
                
                if (thisPlayer != null)
                {
                    thisPlayer.CurrentSession.Close();
                    //вполне возможно, что тут нужен dbContext.SaveChanges(), иначе только что закрытая сессия не подхвтатится
                    if (dbContext.PlayerSessions.Count(c=>c.PlayerId==uID && c.Finished==null)==0)
                    {
                        Battle battle = dbContext.Battles.FirstOrDefault(b => b.Mode == BattleMode.vsPlayer
                                                                       && b.Status == BattleStatus.InAction
                                                                       && (b.Player1Id == thisPlayer.Id || b.Player2Id == thisPlayer.Id));


                        if (battle != null)
                        {
                            battle.Finished = DateTime.Now;
                            battle.BattleResult = thisPlayer.Id==battle.Player1Id?BattleOutcome.Player1Disconnected:BattleOutcome.Player2Disconnected;
                            battle.Status = BattleStatus.Finished;
                            if (battle.Mode == BattleMode.vsPlayer)
                            {
                                string opponentId = battle.Player1Id == thisPlayer.Id ? battle.Player2Id : battle.Player1Id;
                                PlayerSession opponent = dbContext.PlayerSessions.FirstOrDefault();

                                Clients.Client(opponent.ConnectionId).onGameover(thisPlayer.Nickname, battle.BattleResult);
                                Groups.Remove(opponent.ConnectionId, battle.BattleToken.ToString());
                                Groups.Remove(Context.ConnectionId, battle.BattleToken.ToString());
                            }
                        }

                        PendingBattle pBattle = dbContext.PendingBattles.Where(pB => (pB.Player1Id == thisPlayer.Id || pB.Player2Id == pB.Player2Id)).ToList().FirstOrDefault(pB=>pB.IsActive);
                        if (pBattle!=null)
                        {
                            if (pBattle.Player1Id==thisPlayer.Id)
                            {
                                pBattle.Player1Status = PlayerOfferStatus.Canceled;
                                pBattle.Player2Status = PlayerOfferStatus.Ignored;
                                Clients.Client(pBattle.Player2.PlayerSessions.Last(ps=>ps.Finished==null).ConnectionId).onOpponentIgnored();
                            }
                            else
                            {
                                pBattle.Player2Status = PlayerOfferStatus.Canceled;
                                pBattle.Player1Status = PlayerOfferStatus.Ignored;
                                Clients.Client(pBattle.Player1.PlayerSessions.Last(ps =>ps.Finished==null).ConnectionId).onOpponentIgnored();
                            }

                            pBattle.Closed = DateTime.Now;
                        }

                        WaitingPlayer pPlayer = dbContext.WaitingPlayers.FirstOrDefault(wp=>wp.PlayerId==thisPlayer.Id);
                        if (pPlayer != null)
                            dbContext.WaitingPlayers.Remove(pPlayer);

                        Clients.AllExcept(Context.ConnectionId).onUserStateChanged(Context.ConnectionId, thisPlayer.Nickname, PlayerStatus.Online, PlayerStatus.Offline);
                    }

                    dbContext.SaveChanges();
                }
            }
            return base.OnDisconnected(stopCalled);
        }
        public override async Task OnConnected()
        {
            using (BattleshipContext dbContext = new BattleshipContext())
            {
                //определяю текущего игрока
                string uID = Context.User.Identity.GetUserId();
                Player thisPlayer = dbContext.Players.FirstOrDefault(p => p.Id == uID);
                Battle battle = dbContext.Battles.FirstOrDefault(b => b.Status == BattleStatus.InAction
                                                                    && b.Finished == null
                                                                    && (b.Player1Id == thisPlayer.Id || b.Player2Id == thisPlayer.Id));


                dbContext.PlayerSessions.Add(new PlayerSession() { PlayerId = thisPlayer.Id, Started = DateTime.Now, ConnectionId = Context.ConnectionId, Status = battle == null ? PlayerStatus.Online : PlayerStatus.InBattle });
                Groups.Add(Context.ConnectionId, uID);

                //смотрю, находится ли он в каком либо бою
                if (battle != null)
                {
                    if (battle.Mode == BattleMode.vsPlayer)
                    {
                        Player opponent = battle.Player1Id == thisPlayer.Id ? battle.Player2 : battle.Player1;
                        await Clients.User(uID).onConnected(true, opponent.Nickname, battle.Started);
                    }
                    else
                    {
                        BattleOutcome otc = thisPlayer.Id == battle.Player1Id ? BattleOutcome.Player1Disconnected : BattleOutcome.Player2Disconnected;
                        battle.Finished = DateTime.Now;
                        battle.BattleResult = otc;
                        battle.Status = BattleStatus.Finished;
                        await Clients.User(uID).onGameover("комплюктер", otc, false);
                        dbContext.Ships.RemoveRange(thisPlayer.Ships);
                        thisPlayer.Ships.Clear();
                    }
                }
                else
                {
                    //смотрю, нет ли готовящегося боя с ним
                    PendingBattle pBattle = dbContext.PendingBattles.Where(pB => (pB.Player1Id == thisPlayer.Id || pB.Player2Id == pB.Player2Id)).ToList().FirstOrDefault(pB => pB.IsActive);
                    //по сути, невозможная ситуация. в onDisconnected рубятся все pending battle
                    if (pBattle != null)
                    {
                        if (pBattle.Player1Id == thisPlayer.Id)
                            Clients.User(uID).onOfferBattle(pBattle.Player2.Nickname);
                        else
                            Clients.User(uID).onOfferBattle(pBattle.Player1.Nickname);
                    }
                }

                dbContext.SaveChanges();
                //посылаю только что зашедшему чуваку список активных игроков
                var otherUsers = dbContext.PlayerSessions.Where(ps => ps.Finished == null && ps.PlayerId != uID)
                                            .ToList()
                                            .Distinct()
                                            .Select(s => new { ConnectionId=s.PlayerId, Nickname=s.Player.Nickname, Status=s.Status }).ToList();

                Clients.User(uID).onGetUserList(otherUsers);
              
                Clients.Users(otherUsers.Select(s => s.ConnectionId).ToList()).onUserStateChanged(uID, thisPlayer.Nickname, PlayerStatus.Offline, PlayerStatus.Online);
            }


            //соединение
            Clients.Client(Context.ConnectionId).onConnected(false);
            await base.OnConnected();
        }
        
    }
}
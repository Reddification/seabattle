﻿using Battleshit3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace Battleshit3.AI
{
    public struct Coordinate
    {
        public short xCoord, yCoord;
    }
    public static class ArtificialIntelligence
    {
        /// <summary>
        /// Сгенерировать следующее место для атаки. Сначала анализируется последняя атака. 
        /// Если ее результат - hit, то ебануть где нибудь рядом, куда еще не ебашил
        /// </summary>
        /// <param name="P"></param>
        /// <returns></returns>
        public static Attack GenerateAttack(List<Attack> previuousAttacks)
        {
            Random R = new Random((int)DateTime.Now.Ticks);
            Thread.Sleep(R.Next(0,1000));//эмуляция раздумья
            //govno. в ситуации
            //1 попадание в середину трехпалубника
            //2 попадание в 
            Attack lastHit = previuousAttacks.Where(a => a.HitResult == HitResult.Destroyed || a.HitResult == HitResult.Hit).OrderBy(a => a.Id).Last();

            if (lastHit!=null)
            {
                if (isReasonable((short)(lastHit.xCoord - 1), lastHit.yCoord, previuousAttacks))
                    return new Attack() { xCoord = (short)(lastHit.xCoord - 1), yCoord = lastHit.yCoord };
                else if (isReasonable((short)(lastHit.xCoord + 1), lastHit.yCoord, previuousAttacks))
                    return new Attack() { xCoord = (short)(lastHit.xCoord + 1), yCoord = lastHit.yCoord };
                else if (isReasonable(lastHit.xCoord, (short)(lastHit.yCoord - 1), previuousAttacks))
                    return new Attack() { xCoord = lastHit.xCoord, yCoord = (short)(lastHit.yCoord - 1) };
                else if (isReasonable(lastHit.xCoord, (short)(lastHit.yCoord + 1), previuousAttacks))
                    return new Attack() { xCoord = lastHit.xCoord, yCoord = (short)(lastHit.yCoord + 1) };
                else return GenerateRandomAttack(previuousAttacks);
            }
            else return GenerateRandomAttack(previuousAttacks);
        }

        private static List<Ship> predictShips(Attack lastHit, ICollection<Attack> attacks)
        {
            //если есть атаки рядом - можно уменьшить количество предположений

            List<Ship> possibleShips = new List<Ship>();

            List<Ship> destroyedShips = new List<Ship>();
            foreach (Attack lastShot in attacks.Where(a => a.HitResult == HitResult.Destroyed))
                destroyedShips.Add(ReconstructDestroyedShip(lastShot, attacks));

            Attack[] closestAttacks = attacks.Where(a => Math.Abs(lastHit.xCoord - a.xCoord) == 1 || Math.Abs(lastHit.yCoord - a.yCoord) == 1).ToArray();
            //if (closestAttacks.Length>0)
            //{
            //    foreach (var item in collection)
            //    {

            //    }
            //    if (!destroyedShips.Any(ds=>ds.ShipUnits.Count==2 && ds.ShipUnits.Any(dssu=>dssu.xCoord==closestAttacks))
            //}
            //else
            //{
            //    List<ShipUnit> possibleShipUnits = new List<ShipUnit>();
            //    for (int axis = 0; axis < 2; axis++)
            //        for (int x =0;x<4;x++)
            //        {
            //            possibleShipUnits.Add(new ShipUnit() { xCoord = lastHit.xCoord + x * axis, yCoord=lastHit})
            //            possibleShips.Add(new Ship());
            //        }
            //}

                return possibleShips;
        }

        private static bool isReasonable(short x, short y, ICollection<Attack> attacks)
        {
            //return true;
            if (x > 0 && y > 0 && x < 10 && y < 10)
            {
                List<Ship> destroyedShips = new List<Ship>();
                foreach (Attack lastShot in attacks.Where(a => a.HitResult == HitResult.Destroyed))
                    destroyedShips.Add(ReconstructDestroyedShip(lastShot, attacks));

                    return !attacks.Any(a =>
                          ((a.HitResult == HitResult.Missed || a.HitResult==HitResult.Hit) && a.xCoord == x && a.yCoord == y)
                         || (destroyedShips.SelectMany(ds=>ds.ShipUnits).Any(dsu=>Math.Abs(dsu.xCoord-x)<=1 || Math.Abs(dsu.yCoord-y)<=1))
                         );
            }
            else return false;
        }
        private static Ship ReconstructDestroyedShip(Attack lastShot, ICollection<Attack> attacks)
        {
            Ship destroyedShip = new Ship();
            destroyedShip.ShipUnits = new List<ShipUnit>() { new ShipUnit() { xCoord = lastShot.xCoord, yCoord = lastShot.yCoord } };

            List<Attack> closestAttacks = attacks.Where(a => a.HitResult == HitResult.Hit &&
                                                            (Math.Abs(a.xCoord - lastShot.xCoord) == 1 || Math.Abs(a.yCoord - lastShot.yCoord) == 1)).ToList();

            if (closestAttacks.Count > 0)
            {
                bool wasHorizontal = closestAttacks.First().yCoord - lastShot.yCoord == 0;

                Attack attack = lastShot;
                do
                {
                    attack = attacks.FirstOrDefault(a => a.HitResult == HitResult.Hit && wasHorizontal ? a.xCoord - attack.xCoord == 1 : a.yCoord - attack.yCoord == 1);
                    if (attack != null)
                        destroyedShip.ShipUnits.Add(new ShipUnit() { xCoord = attack.xCoord, yCoord = attack.yCoord });
                    else break;
                }
                while (true);

                attack = lastShot;
                do
                {
                    attack = attacks.FirstOrDefault(a => a.HitResult == HitResult.Hit && wasHorizontal ? attack.xCoord - a.xCoord == 1 : attack.yCoord - a.yCoord == 1);
                    if (attack != null)
                        destroyedShip.ShipUnits.Add(new ShipUnit() { xCoord = attack.xCoord, yCoord = attack.yCoord });
                    else break;
                }
                while (true);
            }
            return destroyedShip;
        }


        private static Attack GenerateRandomAttack(ICollection<Attack> attacks)
        {
            //восстанавливаю разбитые корабли, чтобы не херачить снова по ним или по точкам в их окрестности
            List<Ship> destroyedShips = new List<Ship>();
            foreach (Attack lastShot in attacks.Where(a=>a.HitResult==HitResult.Destroyed))
                destroyedShips.Add(ReconstructDestroyedShip(lastShot, attacks));

            //есть еще не уничтоженные корабли, нет смысла стрелять их в разбитые части
            List<ShipUnit> wreckedUnits = attacks.Where(a => a.HitResult == HitResult.Hit 
                                                          && !destroyedShips.SelectMany(s => s.ShipUnits).Any(su => su.xCoord == a.xCoord && su.yCoord == a.yCoord))
                                                 .Select(a => new ShipUnit() { xCoord = a.xCoord, yCoord = a.yCoord }).ToList();


            Random R = new Random((int)DateTime.Now.Ticks);

            List<Coordinate> unknownAreas = new List<Coordinate>();
            for (byte x = 0; x < 10; x++)
                for (byte y = 0; y < 10; y++)
                    if (!destroyedShips.SelectMany(s=>s.ShipUnits).Any(su=>Math.Abs(su.xCoord-x)<=1||Math.Abs(su.yCoord-y)<=1) //не в области уничтоженного корабля
                     && !wreckedUnits.Any(wu=>wu.xCoord==x&&wu.yCoord==y))//не на уже подорванной части еще живого корабля
                        unknownAreas.Add(new Coordinate() { xCoord = x, yCoord = y});

            int randomCoord = R.Next(0, unknownAreas.Count);
            return new Attack(){ xCoord=unknownAreas[randomCoord].xCoord, yCoord=unknownAreas[randomCoord].yCoord};
        }

        public static ICollection<Ship> DislocateShips()
        {
            List<Coordinate> battlefield = new List<Coordinate>();
            List<Ship> rezult = new List<Ship>();

            for (byte i = 0; i < 10; i++)
                for (byte j = 0; j < 10; j++)
                    battlefield.Add(new Coordinate() { xCoord = i, yCoord = j });

            rezult.Add(PlaceShip(4, battlefield));
            rezult.Add(PlaceShip(3, battlefield));
            rezult.Add(PlaceShip(3, battlefield));
            rezult.Add(PlaceShip(2, battlefield));
            rezult.Add(PlaceShip(2, battlefield));
            rezult.Add(PlaceShip(2, battlefield));
            rezult.Add(PlaceShip(1, battlefield));
            rezult.Add(PlaceShip(1, battlefield));
            rezult.Add(PlaceShip(1, battlefield));
            rezult.Add(PlaceShip(1, battlefield));

            return rezult;
        }

        private static Ship PlaceShip(int length, List<Coordinate> battlefield)
        {
            Random R = new Random((int)DateTime.Now.Ticks);
            bool isHorizontal= R.Next(100)>50;

            var b1 = battlefield.Where(b => isHorizontal ? b.xCoord <= 10-length : b.yCoord <= 10-length 
                                         && length>1? battlefield.Where(ib=>isHorizontal ? 
                                                                                 (ib.xCoord>b.xCoord && b.xCoord+length-ib.xCoord>0)
                                                                               : (ib.yCoord > b.yCoord && b.yCoord + length - ib.yCoord > 0)).Count()==length-1:true).ToList();

            int index = R.Next(10);
            Coordinate origin = new Coordinate() { xCoord = b1[index].xCoord, yCoord = b1[index].yCoord };



            List<ShipUnit> SUs = new List<ShipUnit>();
            for (int i = 0; i < length; i++)
                SUs.Add(new ShipUnit() { xCoord = isHorizontal ? (byte)(origin.xCoord + i) : origin.xCoord, yCoord = isHorizontal ? origin.yCoord : (byte)(origin.yCoord + i) });

            battlefield.RemoveAll(b => SUs.Any(su => Math.Abs(b.xCoord - su.xCoord) <= 1 || Math.Abs(b.yCoord - su.yCoord) <= 1));
            return new Ship() { ShipUnits = SUs };
        }

    }
    public static partial class Extensions
    {
        public static void AddRange<T>(this ICollection<T> destination, IEnumerable<T> source)
        {
            List<T> list = destination as List<T>;

            if (list != null)
            {
                list.AddRange(source);
            }
            else
            {
                foreach (T item in source)
                {
                    destination.Add(item);
                }
            }
        }
    }
}